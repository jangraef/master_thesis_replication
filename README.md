# Master_Thesis_Replication

This repository contains the Stata do-file to generate all findings presented in the thesis *Continuity and Change in the Living Arrangements of Ghanaian Children*.

## Data Sources

* Ghana Demographic and Health Surveys 1993 - 2014 (Standard DHS) (GDHS; https://dhsprogram.com/data/available-datasets.cfm) are available for scientific purposes after approval of an application form.
* ISSER-Northwestern-Yale Long Term Ghana Socioeconomic Panel Survey 2017/2018 (Wave 3) (GSPS; https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/E5QP0F&version=1.2) is publicly available without application.

## Repository Contents

This repository contains the following files:

* Replication_Masterthesis_GRAEF.do - A Stata do-file that contains the complete code used to harmonize and append all data sources, to run all statistical analyses and to export the corresponding results as displayed in the thesis. 
* Master_Thesis_Replication.zip (Folder structure) - In the do-file, a specific folder structure is assumed. This exact folder structure is needed for all commands to run without error. Make sure to copy it to your working directory and also make sure to change the working directory in the first section of the do-file.

## Instructions for Replication

1. Download the folder structure into a main folder.
2. Obtain the data and save into the corresponding folders.
3. Open the do-file (01_dofiles\Replication_Masterthesis_GRAEF.do) and provide the working directory after "global wdir ..." (line 21)
4. Run the do-file (01_dofiles\Replication_Masterthesis_GRAEF.do).