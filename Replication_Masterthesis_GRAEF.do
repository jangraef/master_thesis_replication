/*==============================================================================
File name:    Replication_Masterthesis_GRAEF.do
Class:        Masterthesis Module
Task:  		  Replication File
Author:		  Jan Graef 
Last update:  05.08.2023
==============================================================================*/

/*------------------------------------------------------------------------------
I Configuration
------------------------------------------------------------------------------*/

* Settings
version 17.0           
clear all             
macro drop _all 
set maxvar 10000    

* Working directory
global wdir  "C:\Users\User\Desktop\MT_Data"

* Define paths to subdirectories 
global data_1993 	"$wdir/0_data/GDHS/1993"	// GDHS 1993
global data_1998 	"$wdir/0_data/GDHS/1998"	// GDHS 1998
global data_2003 	"$wdir/0_data/GDHS/2003"	// GDHS 2003
global data_2008 	"$wdir/0_data/GDHS/2008"	// GDHS 2008
global data_2014 	"$wdir/0_data/GDHS/2014"	// GDHS 2014
global data_2018 	"$wdir/0_data/GSPS/2018"  	// GSPS 2018
global code 		"$wdir/1_dofiles"   		// folder for do-files
global posted 		"$wdir/2_posted"    		// data ready for analysis
global temp 		"$wdir/3_temp"   			// folder for temporary files
global table		"$wdir/4_tables" 			// folder for table output 
global graph		"$wdir/5_graphs" 			// folder for graph output 

* Install packages
ssc install combomarginsplot
ssc install elabel
ssc install fre
net install grc1leg2, from("http://digital.cgdev.org/doc/stata/MO/Misc/")


/*------------------------------------------------------------------------------
II Data Harmonization & Preparation
------------------------------------------------------------------------------*/

**#

/*------------------------------------------------------------------------------
 1993 (GDHS)
------------------------------------------------------------------------------*/

**#

* MERGING DATASETS

* Rename identifier variables in men dataset in order to be able to merge
use "$data_1993/men.dta", clear
rename (mv001 mv002 mv003) (v001 v002 v003)
save "$data_1993/men_id_edit.dta", replace

* Rename identifier variables in wealth dataset in order to be able to merge
use "$data_1993/wealth.dta", clear
rename whhid hhid
save "$data_1993/wealth_id_edit.dta", replace

* Open householdmember dataset which serves as master data
use "$data_1993/householdmember.dta", clear 

* Wave identifier
gen wave = 1993

* Cluster number
rename hv001 v001

* Household number
rename hv002 v002

* Respondent's line number
rename hvidx v003

* Merge men and women data to household member data
merge 1:1 v001 v002 v003 using "$data_1993/individual", nogen 
merge 1:1 v001 v002 v003 using "$data_1993/men_id_edit", nogen

* Merge wealth data to household member data
merge m:1 hhid using "$data_1993/wealth_id_edit", nogen

* Weighting variables

* (i) Household weight
fre hv005 // self-weighting sample design
gen wgt = hv005/1000000
label var wgt "Household Weight"

* (ii) Cluster (PSU)
fre hv021 v001 // equal -> both can be used
gen clstr_1993 = hv021
label var clstr_1993 "Cluster"

* (iii) Stratum
fre hv023 // only national
fre hv024 hv025 // only national
egen stratum_1993 = group(hv024 hv025), label // 20 strata
label var stratum_1993 "Stratum"


* SOCIODEMOGRAPHIC VARIABLES

* Region
fre hv024
/*		1  western
        2  central
        3  greater accra
        4  volta
        5  eastern
        6  ashanti
        7  brong-ahafo
		8  northern
		9  upper west
		10 upper east
*/
rename hv024 region
label def region					///
		1  "Western"				///
        2  "Central"				///
        3  "Greater Accra"			///
        4  "Volta"					///
        5  "Eastern"				///
        6  "Ashanti"				///
        7  "Brong-Ahafo"			///
		8  "Northern"				///
		9  "Upper West"				///
		10 "Upper East"
label val region region

* Type of place of residency
fre hv025
/*		1  urban
        2  rural
*/
rename hv025 urbanrural
label def urbanrural 			///
	1 "Urban"					///
	2 "Rural"
label val urbanrural urbanrural

* Ethnicity
fre v131 // (women; 14 < 50)
/*		 1  asante
         2  akwapim
         3  fanti
         4  other akan
         5  ga.adangbe
         6  ewe
         7  guan
         8  mole-dagbani
         9  grussi
         10 gruma
         11 hausa
         12 other
*/

fre mv131 // (men; 14 < 50)
/*		 1  asante
         2  akwapin
         3  fanti
         4  other akan
         5  ga /adangbe
         6  ewe
         7  guan
         8  mole-dagbani
         9  grussi
         10 gruma
         11 hausa
         12 central togo tribes
         13 other ghanaian tr.
         14 other non-gh.tribes
         96 other
*/

gen ethnicity = .
replace ethnicity = 1 if v131 < 5 | mv131 < 5
replace ethnicity = 2 if v131 == 5 | mv131 == 5
replace ethnicity = 3 if v131 == 6 | mv131 == 6
replace ethnicity = 4 if v131 == 7 | mv131 == 7
replace ethnicity = 5 if v131 == 8 | mv131 == 8
replace ethnicity = 6 if v131 == 9 | mv131 == 9
replace ethnicity = 7 if v131 == 10 | mv131 == 10
replace ethnicity = 8 if (v131 > 10 & v131 < .) | (mv131 > 10 & mv131 < .)
label var ethnicity "Ethnicity"
label def ethnicity							///
		 1  "Akan"							///
         2  "Ga/Adangbe"					///
         3  "Ewe"							///
         4  "Guan"							///
         5  "Mole-Dagbani"					///
         6  "Grussi"						///
         7  "Gruma"							///
         8  "Other"
label val ethnicity ethnicity


* Sex
fre hv104
/*		1  male
        2  female
*/
rename hv104 sex
label def sex 		///
	1 "Male"		///
	2 "Female"			
label val sex sex

* Age
fre hv105
/* 		Numerical
		96  96+
		98	dk
*/
recode hv105 (96 = 95 "95+") (98=.), gen(age)
label var age "Age"

* Relationship to head
fre hv101
/*		1  head
        2  wife or husband
        3  son/daughter
        4  son/daughter-in-law
        5  grandchild
        6  parent
        7  parent-in-law
		8  brother/sister
		9  co-spouse
		10 other relative
		11 adopted/foster child
		12 not related
*/
rename hv101 relationship

* Usual resident
fre hv102
/*		0  no
        1  yes
*/
rename hv102 defacto


* Highest educational level 
fre hv106 
/*		0  no education
		1  primary
        2  secondary
        3  higher
        8  dk
*/
recode hv106 (8=.)
rename hv106 educat

* Mother alive (only data for individuals < 15)
fre hv111
/*		0  no
        1  yes
		8  dk
*/
recode hv111 (8=.)
rename hv111 motheralive

* Mother's line number (only data for individuals < 15)
fre hv112 
/*		0  mother not in hh
        Numerical
*/
rename hv112 motherhhmid

* Father alive (only data for individuals < 15)
fre hv113
/*		0  no
        1  yes
		8  dk
*/
recode hv113 (8=.)
rename hv113 fatheralive

* Father's line number (only data for individuals < 15)
fre hv114 
/*	    0  father not in hh
        Numerical
*/
rename hv114 fatherhhmid

* Currently, formerly never married
fre hv116
/*		0  never married
		1  currently married (married + living together from hv115)
        2  formerly/ever married (widowed + divorced + not living together)
*/
rename hv116 gmaritalstatus
label def mstat 			///
	0 "Never married"		///
	1 "Cohabiting/married"	///
	2 "Formerly married"
label val gmaritalstatus mstat
	

* Currently working
fre v714 // (women 14 < 50)
/*		0  no
        1  yes
*/
fre mv714 // (men 14 < 60)
/*		0  no
        1  yes
*/

gen cwork = .
replace cwork = 0 if v714 == 0 | mv714 == 0
replace cwork = 1 if v714 == 1 | mv714 == 1
label var cwork "currently working"
label def cwork 0 "No" 1 "Yes"
label val cwork cwork

* Wealth
fre wlthind5
*		numerical (quintiles)
rename wlthind5 wealthindexquint
label def wealthindexquint 		///
	1	"Lowest"				///
	2	"Second"				///
	3	"Third"					///
	4	"Fourth"				///
	5	"Highest"
label val wealthindexquint wealthindexquint

* Religion
fre v130 // (women 14 < 50)
/*		   0   no religion
           1   protestant
           2   catholic
           3   other christian
           4   muslim
           5   traditional
           6   other


*/
fre mv130 // (men 14 < 60)
/*		   0   no religion
           1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   pentecostal
           6   spiritualist
           7   other christian
           8   moslem
           9   traditional


*/
gen religion = .
replace religion = 1 if (v130 > 0 & v130 < 4) | (mv130 > 0 & mv130 < 8)
replace religion = 2 if (v130 == 4 | mv130 == 8)
replace religion = 3 if (v130 == 5 | mv130 == 9)
replace religion = 4 if (v130 == 0 | v130 == 6) | mv130 == 0
label var religion "Religion"
label def religion						///
	1 "Christian"						///
	2 "Muslim"							///
	3 "Traditional"						///
	4 "None/Other" 						
label val religion religion


* Consecutive household id variable
order wave v001 v002 v003
egen picktwo = tag(v001 v002)
drop hhid
gen hhid = sum(picktwo)


keep 																///
	wave v001 v002 v003	hhid wgt clstr_1993 stratum_1993			///	
	region urbanrural ethnicity										///
	relationship defacto	   										///
	sex age educat													///
	motheralive motherhhmid fatheralive fatherhhmid 				///
	gmaritalstatus													///
	cwork religion													///
	wealthindexquint														
	

save "$temp/gdhs_1993", replace


/*------------------------------------------------------------------------------
 1998 (GDHS)
------------------------------------------------------------------------------*/

**#

* MERGING DATASETS

* Rename identifier variables in men dataset in order to be able to merge
use "$data_1998/men.dta", clear
rename (mv001 mv002 mv003) (v001 v002 v003)
save "$data_1998/men_id_edit.dta", replace

* Rename identifier variables in wealth dataset in order to be able to merge
use "$data_1998/wealth.dta", clear
rename whhid hhid
save "$data_1998/wealth_id_edit.dta", replace

* Open householdmember dataset which serves as master data
use "$data_1998/householdmember.dta", clear 

* Wave identifier
gen wave = 1998

* Cluster number
rename hv001 v001

* Household number
rename hv002 v002

* Respondent's line number
rename hvidx v003

* Merge men and women data to household member data
merge 1:1 v001 v002 v003 using "$data_1998/individual", nogen 
merge 1:1 v001 v002 v003 using "$data_1998/men_id_edit", nogen 

* Merge wealth data to household member data
merge m:1 hhid using "$data_1998/wealth_id_edit", nogen 

* Weighting variables

* (i) Household weight
fre hv005
gen wgt = hv005/1000000
label var wgt "Household Weight"

* (ii) Cluster (PSU)
fre hv021 v001 // equal -> both can be used
gen clstr_1998 = hv021
label var clstr_1998 "Cluster"

* (iii) Stratum
fre hv023
clonevar stratum_1998 = hv023 // 20 strata
label var stratum_1998 "Stratum"


* SOCIODEMOGRAPHIC VARIABLES

* Region
fre hv024
/*		1  western
        2  central
        3  greater accra
        4  volta
        5  eastern
        6  ashanti
        7  brong-ahafo
		8  northern
		9  upper west
		10 upper east
*/
rename hv024 region

* Type of place of residency
fre hv025
/*		1  urban
        2  rural
*/
rename hv025 urbanrural

* Ethnicity
fre v131 // (women; 14 < 50)
/*		 1  asante
         2  akwapim
         3  fanti
         4  other akan
         5  ga.adangbe
         6  ewe
         7  guan
         8  mole-dagbani
         9  grussi
         10 gruma
         11 hausa
         12 dagarti
		 96 other
*/

fre mv131 // (men; 14 < 50)
/*		 1  asante
         2  akwapim
         3  fanti
         4  other akan
         5  ga.adangbe
         6  ewe
         7  guan
         8  mole-dagbani
         9  grussi
         10 gruma
         11 hausa
         12 dagarti
		 96 other
*/

gen ethnicity = .
replace ethnicity = 1 if v131 < 5 | mv131 < 5
replace ethnicity = 2 if v131 == 5 | mv131 == 5
replace ethnicity = 3 if v131 == 6 | mv131 == 6
replace ethnicity = 4 if v131 == 7 | mv131 == 7
replace ethnicity = 5 if v131 == 8 | mv131 == 8
replace ethnicity = 6 if v131 == 9 | mv131 == 9
replace ethnicity = 7 if v131 == 10 | mv131 == 10
replace ethnicity = 8 if (v131 > 10 & v131 < .) | (mv131 > 10 & mv131 < .)
label var ethnicity "Ethnicity"
label def ethnicity							///
		 1  "Akan"							///
         2  "Ga/Adangbe"					///
         3  "Ewe"							///
         4  "Guan"							///
         5  "Mole-Dagbani"					///
         6  "Grussi"						///
         7  "Gruma"							///
         8 	"Other"	
label val ethnicity ethnicity

* Relationship to head
fre hv101
/*		1  head
        2  wife or husband
        3  son/daughter
        4  son/daughter-in-law
        5  grandchild
        6  parent
        7  parent-in-law
		8  brother/sister
		9  co-spouse
		10 other relative
		11 adopted/foster child
		12 not related
*/
rename hv101 relationship

* Usual resident
fre hv102
/*		0  no
        1  yes
*/
rename hv102 defacto

* Sex
fre hv104
/*		1  male
        2  female
*/
rename hv104 sex

* Age
fre hv105
/* 		Numerical
		97  97+
		98	dk
*/
recode hv105 (96 97 = 95 "95+") (98=.), gen(age)
label var age "Age"

* Highest educational level 
fre hv106 
/*		0  no education
		1  primary
        2  secondary
        3  higher
        8  dk
*/
recode hv106 (8=.)
rename hv106 educat

* Mother alive (only data for individuals < 15)
fre hv111
/*		0  no
        1  yes
		8  dk
*/
recode hv111 (8=.)
rename hv111 motheralive

* Mother's line number (only data for individuals < 15)
fre hv112 
/*		0  mother not in hh
        Numerical
*/
rename hv112 motherhhmid

* Father alive (only data for individuals < 15)
fre hv113
/*		0  no
        1  yes
		8  dk
*/
recode hv113 (8=.)
rename hv113 fatheralive

* Father's line number (only data for individuals < 15)
fre hv114 
/*	    0  father not in hh
        Numerical
*/
rename hv114 fatherhhmid

* Currently, formerly never married
fre hv116
/*		0  never married
		1  currently married (married + living together from hv115)
        2  formerly/ever married (widowed + divorced + not living together)
*/
rename hv116 gmaritalstatus
label def mstat 			///
	0 "Never married"		///
	1 "Cohabiting/married"	///
	2 "Formerly married"
label val gmaritalstatus mstat

* Currently working
fre v714 // (women 14 < 50)
/*		0  no
        1  yes
*/
fre mv714 // (men 14 < 60)
/*		0  no
        1  yes
*/

gen cwork = .
replace cwork = 0 if v714 == 0 | mv714 == 0
replace cwork = 1 if v714 == 1 | mv714 == 1
label var cwork "currently working"
label def cwork 0 "no" 1 "yes"
label val cwork cwork

* Wealth
fre wlthind5
*		numerical (quintiles)
rename wlthind5 wealthindexquint

* Religion
fre v130 // (women 14 < 50)
/*		   0   no religion + other
           1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   spiritualist
           6   other christian
           7   moslem
           8   traditional
*/
fre mv130 // (men 14 < 60)
/*		   0   no religion + other
           1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   spiritualist
           6   other christian
           7   moslem
           8   traditional
*/
gen religion = .
replace religion = 1 if (v130 > 0 & v130 < 7) | (mv130 > 0 & mv130 < 7)
replace religion = 2 if (v130 == 7 | mv130 == 7)
replace religion = 3 if (v130 == 8 | mv130 == 8)
replace religion = 4 if v130 == 0 | mv130 == 0
label var religion "Religion"
label def religion						///
	1 "Christian"						///
	2 "Muslim"							///
	3 "Traditional"						///
	4 "None/Other" 						
label val religion religion

* Consecutive household id variable
order wave v001 v002 v003
egen picktwo = tag(v001 v002)
drop hhid
gen hhid = sum(picktwo)


keep 																///
	wave v001 v002 v003	hhid wgt clstr_1998 stratum_1998			///	
	region urbanrural ethnicity										///
	relationship defacto	   										///
	sex age educat 													///
	motheralive motherhhmid fatheralive fatherhhmid 				///
	gmaritalstatus													///
	cwork religion		 											///
	wealthindexquint														
	

save "$temp/gdhs_1998", replace


/*------------------------------------------------------------------------------
 2003 (GDHS)
------------------------------------------------------------------------------*/

**#

* MERGING DATASETS

* Rename identifier variables in men dataset in order to be able to merge
use "$data_2003/men.dta", clear
rename (mv001 mv002 mv003) (v001 v002 v003)
save "$data_2003/men_id_edit.dta", replace

* Open householdmember dataset which serves as master data
use "$data_2003/householdmember.dta", clear 

* Wave identifier
gen wave = 2003

* Cluster number
rename hv001 v001

* Household number
rename hv002 v002

* Respondent's line number
rename hvidx v003

* Merge men and women data to household member data
merge 1:1 v001 v002 v003 using "$data_2003/individual", nogen 
merge 1:1 v001 v002 v003 using "$data_2003/men_id_edit", nogen 

* Merge wealth data to household member data
merge m:1 hhid using "$data_2003/household", nogen 

* Weighting variables

* (i) Household weight
fre hv005
gen wgt = hv005/1000000
label var wgt "Household Weight"

* (ii) Cluster (PSU)
fre hv021 v001 // equal -> both can be used
gen clstr_2003 = hv021
label var clstr_2003 "Cluster"

* (iii) Stratum
fre hv023 // only region, urban/rural info missing
fre hv024 hv025
egen stratum_2003 = group(hv024 hv025), label // 20 strata
label var stratum_2003 "Stratum"


* SOCIODEMOGRAPHIC VARIABLES

* Region
fre hv024
/*		1  western
        2  central
        3  greater accra
        4  volta
        5  eastern
        6  ashanti
        7  brong-ahafo
		8  northern
		9  upper west
		10 upper east
*/
rename hv024 region

* Type of place of residency
fre hv025
/*		1  urban
        2  rural
*/
rename hv025 urbanrural

* Ethnicity
fre v131 // (women; 14 < 50)
/*		1   akan
        2   ga/dangme
        3   ewe
        4   guan
        5   mole-dagbani
        6   grussi
        7   gruma
        8   hausa
        96  other
		99
*/
recode v131 (96=8) (99 = .)

fre mv131 // (men; 14 < 50)
/*		 1  akan
         2  ga/dangme
         3  ewe
         4  guan
         5  mole-dagbani
         6  grussi
         7  gruma
         8  hausa
         96 other
		 99
*/
recode mv131 (96=8) (99 = .)

gen ethnicity = .
replace ethnicity = v131 if v131 < .
replace ethnicity = mv131 if mv131 < .
label var ethnicity "Ethnicity"
label def ethnicity							///
		 1  "Akan"							///
         2  "Ga/Adangbe"					///
         3  "Ewe"							///
         4  "Guan"							///
         5  "Mole-Dagbani"					///
         6  "Grussi"						///
         7  "Gruma"							///
         8  "Other"	
label val ethnicity ethnicity

* Relationship to head
fre hv101
/*		1  head
        2  wife or husband
        3  son/daughter
        4  son/daughter-in-law
        5  grandchild
        6  parent
        7  parent-in-law
		8  brother/sister
		9  co-spouse
		10 other relative
		11 adopted/foster child
		12 not related
		99
*/
recode hv101 (99=.)
rename hv101 relationship

* Usual resident
fre hv102
/*		0  no
        1  yes
		9
*/
recode hv102 (9=.)
rename hv102 defacto

* Sex
fre hv104
/*		1  male
        2  female
*/
rename hv104 sex

* Age
fre hv105
/* 		Numerical
		97  97+
		98	dk
		99
*/
recode hv105 (96 97 = 95 "95+") (98 99=.), gen(age)
label var age "Age"

* Highest educational level 
fre hv106 
/*		0  no education, preschool
		1  primary
        2  secondary
        3  higher
        8  dk
		9
*/
recode hv106 (8 9=.)
rename hv106 educat

* Mother alive (only data for individuals < 15)
fre hv111
/*		0  no
        1  yes
		8  dk
		9
*/
recode hv111 (8 9 =.)
rename hv111 motheralive

* Mother's line number (only data for individuals < 15)
fre hv112 
/*		0  mother not in hh
        Numerical
		99
*/
recode hv112 (99 =.)
rename hv112 motherhhmid

* Father alive (only data for individuals < 15)
fre hv113
/*		0  no
        1  yes
		8  dk
		9
*/
recode hv113 (8 9=.)
rename hv113 fatheralive

* Father's line number (only data for individuals < 15)
fre hv114 
/*	    0  father not in hh
        Numerical
*/
recode hv114 (99 =.)
rename hv114 fatherhhmid

* Currently, formerly never married
fre v502 mv502
/*		0  never married
		1  currently married (married + living together from hv115)
        2  formerly/ever married (widowed + divorced + not living together)
*/
gen gmaritalstatus = .
replace gmaritalstatus = v502 if v502 < .
replace gmaritalstatus = mv502 if mv502 < .
label var gmaritalstatus "Currently, formerly never married"
label def mstat 			///
	0 "Never married"		///
	1 "Cohabiting/married"	///
	2 "Formerly married"
label val gmaritalstatus mstat	


* Currently working
fre v714 // (women 14 < 50)
/*		0  no
        1  yes
		9
*/
recode v714 (9 =.)

fre mv714 // (men 14 < 60)
/*		0  no
        1  yes
		9
*/
recode mv714 (9 =.)

gen cwork = .
replace cwork = 0 if v714 == 0 | mv714 == 0
replace cwork = 1 if v714 == 1 | mv714 == 1
label var cwork "currently working"
label def cwork 0 "no" 1 "yes"
label val cwork cwork

* Wealth
fre hv270
/*		1   poorest
        2   poorer
        3   middle
        4   richer
        5   richest
*/
rename hv270 wealthindexquint
* Harmonise with labels for other waves
label def wealthindexquint			///
		1   "lowest quintile"		///
        2   "second quintile"		///
        3   "middle quintile"		///
        4   "fourth quintile"		///
        5   "highest quintile"		
label val wealthindexquint wealthindexquint	

* Religion
fre v130 // (women 14 < 50)
/*		   0   no religion
           1   roman catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   other christian
           6   moslem
           7   traditional/spiritualist
           8   other
*/
fre mv130 // (men 14 < 60)
/*		   0   none
           1   roman catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   other christian
           6   moslem
           7   traditional/spiritualist
           8   other
*/
gen religion = .
replace religion = 1 if (v130 > 0 & v130 < 6) | (mv130 > 0 & mv130 < 6)
replace religion = 2 if (v130 == 6 | mv130 == 6)
replace religion = 3 if (v130 == 7 | mv130 == 7)
replace religion = 4 if v130 == 0 | v130 == 8 | mv130 == 0 | mv130 == 8
label var religion "Religion"
label def religion						///
	1 "Christian"						///
	2 "Muslim"							///
	3 "Traditional"						///
	4 "None/Other" 						
label val religion religion

* Consecutive household id variable
order wave v001 v002 v003
egen picktwo = tag(v001 v002)
drop hhid
gen hhid = sum(picktwo)


keep 																///
	wave v001 v002 v003	hhid wgt clstr_2003 stratum_2003			///	
	region urbanrural ethnicity										///
	relationship defacto	   										///
	sex age educat 													///
	motheralive motherhhmid fatheralive fatherhhmid 				///
	gmaritalstatus													///
	cwork religion		 											///
	wealthindexquint										
	

save "$temp/gdhs_2003", replace


/*------------------------------------------------------------------------------
 2008 (GDHS)
------------------------------------------------------------------------------*/

**#

* MERGING DATASETS

* Rename identifier variables in men dataset in order to be able to merge
use "$data_2008/men.dta", clear
rename (mv001 mv002 mv003) (v001 v002 v003)
save "$data_2008/men_id_edit.dta", replace

* Open householdmember dataset which serves as master data
use "$data_2008/householdmember.dta", clear 

* Wave identifier
gen wave = 2008

* Cluster number
rename hv001 v001

* Household number
rename hv002 v002

* Respondent's line number
rename hvidx v003

* Merge men and women data to household member data
merge 1:1 v001 v002 v003 using "$data_2008/individual", nogen 
merge 1:1 v001 v002 v003 using "$data_2008/men_id_edit", nogen 

* Merge wealth data to household member data
merge m:1 hhid using "$data_2008/household", nogen

* Weighting variables

* (i) Household weight
fre hv005
gen wgt = hv005/1000000
label var wgt "Household Weight"

* (ii) Cluster (PSU)
fre hv021 v001 // equal -> both can be used
gen clstr_2008 = hv021
label var clstr_2008 "Cluster"

* (iii) Stratum
fre hv023 // only region, urban/rural info missing
fre hv024 hv025
egen stratum_2008 = group(hv024 hv025), label // 20 strata
label var stratum_2008 "Stratum"


* SOCIODEMOGRAPHIC VARIABLES

* Region
fre hv024
/*		1  western
        2  central
        3  greater accra
        4  volta
        5  eastern
        6  ashanti
        7  brong-ahafo
		8  northern
		9  upper west
		10 upper east
*/
rename hv024 region

* Type of place of residency
fre hv025
/*		1  urban
        2  rural
*/
rename hv025 urbanrural

* Ethnicity
fre v131 // (women; 14 < 50)
/*		1   akan
        2   ga/dangme
        3   ewe
        4   guan
        5   mole-dagbani
        6   grussi
        7   gruma
        8   mande
        96  other
		99
*/
recode v131 (96=8) (99 = .)

fre mv131 // (men; 14 < 50)
/*		 1  akan
         2  ga/dangme
         3  ewe
         4  guan
         5  mole-dagbani
         6  grussi
         7  gruma
         8  hausa
         96 other
		 99
*/
recode mv131 (96=8) (99 = .)

gen ethnicity = .
replace ethnicity = v131 if v131 < .
replace ethnicity = mv131 if mv131 < .
label var ethnicity "Ethnicity"
label def ethnicity							///
		 1  "Akan"							///
         2  "Ga/Adangbe"					///
         3  "Ewe"							///
         4  "Guan"							///
         5  "Mole-Dagbani"					///
         6  "Grussi"						///
         7  "Gruma"							///
         8 	"Other"	
label val ethnicity ethnicity

* Relationship to head
fre hv101
/*		1  head
        2  wife or husband
        3  son/daughter
        4  son/daughter-in-law
        5  grandchild
        6  parent
        7  parent-in-law
		8  brother/sister
		9  co-spouse
		10 other relative
		11 adopted/foster child
		12 not related
		13 niece/nephew by blood 
		14 niece/nephew by marriage
*/
recode hv101 (13 14 = 10)
rename hv101 relationship

* Usual resident
fre hv102
/*		0  no
        1  yes
		9
*/
recode hv102 (9=.)
rename hv102 defacto

* Sex
fre hv104
/*		1  male
        2  female
*/
rename hv104 sex

* Age
fre hv105
/* 		Numerical
		97  97+
		98	dk
		99
*/
recode hv105 (96 = 95 "95+") (98 99=.), gen(age)
label var age "Age"

* Highest educational level 
fre hv106 
/*		0  no education, preschool
		1  primary
        2  secondary
        3  higher
        8  dk
		9
*/
recode hv106 (8 9=.)
rename hv106 educat

* Mother alive (only data for individuals < 15)
fre hv111
/*		0  no
        1  yes
		8  dont know
		9
*/
recode hv111 (8 9 =.)
rename hv111 motheralive

* Mother's line number (only data for individuals < 15)
fre hv112 
/*		0  mother not in hh
        Numerical
		99
*/
recode hv112 (99 =.)
rename hv112 motherhhmid

* Father alive (only data for individuals < 15)
fre hv113
/*		0  no
        1  yes
		8  dont know
		9
*/
recode hv113 (8 9=.)
rename hv113 fatheralive

* Father's line number (only data for individuals < 15)
fre hv114 
/*	    0  father not in hh
        Numerical
*/
rename hv114 fatherhhmid

* Currently, formerly never married
fre hv116
/*		0  never married
		1  currently married (married + living together from hv115)
        2  formerly/ever married (widowed + divorced + not living together)
		9
*/
recode hv116 (9 =.)
rename hv116 gmaritalstatus
label def mstat 			///
	0 "Never married"		///
	1 "Cohabiting/married"	///
	2 "Formerly married"
label val gmaritalstatus mstat

* Currently working
fre v714 // (women 14 < 50)
/*		0  no
        1  yes
		9
*/
recode v714 (9 =.)

fre mv714 // (men 14 < 60)
/*		0  no
        1  yes
		9
*/
recode mv714 (9 =.)

gen cwork = .
replace cwork = 0 if v714 == 0 | mv714 == 0
replace cwork = 1 if v714 == 1 | mv714 == 1
label var cwork "currently working"
label def cwork 0 "no" 1 "yes"
label val cwork cwork

* Wealth
fre hv270
/*		1   poorest
        2   poorer
        3   middle
        4   richer
        5   richest
*/
rename hv270 wealthindexquint
* Harmonise with labels for other waves
label def wealthindexquint			///
		1   "lowest quintile"		///
        2   "second quintile"		///
        3   "middle quintile"		///
        4   "fourth quintile"		///
        5   "highest quintile"		
label val wealthindexquint wealthindexquint	

* Religion
fre v130 // (women 14 < 50)
/*		   1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   pentecostal/charismatic
           6   other christian
           7   moslem
           8   traditional/spiritualist
           9   no religion
          96   other
*/
fre mv130 // (men 14 < 60)
/*		   1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   pentecostal/charismatic
           6   other christian
           7   moslem
           8   traditional/spiritualist
           9   no religion
          96   other
*/
gen religion = .
replace religion = 1 if (v130 > 0 & v130 < 7) | (mv130 > 0 & mv130 < 7)
replace religion = 2 if (v130 == 7 | mv130 == 7)
replace religion = 3 if (v130 == 8 | mv130 == 8)
replace religion = 4 if v130 == 9 | v130 == 96 | mv130 == 9 | mv130 == 96
label var religion "Religion"
label def religion						///
	1 "Christian"						///
	2 "Muslim"							///
	3 "Traditional"						///
	4 "None/Other" 						
label val religion religion

* Consecutive household id variable
egen picktwo = tag(v001 v002)
drop hhid
gen hhid = sum(picktwo)
order wave v001 v002 v003 picktwo hhid

keep 																///
	wave v001 v002 v003	hhid wgt clstr_2008 stratum_2008			///	
	region urbanrural ethnicity										///
	relationship defacto	   										///
	sex age educat													///
	motheralive motherhhmid fatheralive fatherhhmid 				///
	gmaritalstatus 													///
	cwork religion													///
	wealthindexquint		

	
save "$temp/gdhs_2008", replace


/*------------------------------------------------------------------------------
 2014 (GDHS)
------------------------------------------------------------------------------*/

**#

* MERGING DATASETS

* Rename identifier variables in men dataset in order to be able to merge
use "$data_2014/men.dta", clear
rename (mv001 mv002 mv003) (v001 v002 v003)
save "$data_2014/men_id_edit.dta", replace

* Open householdmember dataset which serves as master data
use "$data_2014/householdmember.dta", clear 

* Wave identifier
gen wave = 2014

* Cluster number
rename hv001 v001

* Household number
rename hv002 v002

* Respondent's line number
rename hvidx v003

* Merge men and women data to household member data
merge 1:1 v001 v002 v003 using "$data_2014/individual", nogen 
merge 1:1 v001 v002 v003 using "$data_2014/men_id_edit", nogen 

* Merge wealth data to household member data
merge m:1 hhid using "$data_2014/household", nogen

* (i) Household weight
fre hv005
gen wgt = hv005/1000000
label var wgt "Household Weight"

* (ii) Cluster (PSU)
fre hv021 v001 // equal -> both can be used
gen clstr_2014 = hv021
label var clstr_2014 "Cluster"

* (iii) Stratum
fre hv023 
clonevar stratum_2014 = hv023 // 20 strata
label var stratum_2014 "Stratum"


* SOCIODEMOGRAPHIC VARIABLES

* Region
fre hv024
/*		1  western
        2  central
        3  greater accra
        4  volta
        5  eastern
        6  ashanti
        7  brong-ahafo
		8  northern
		9  upper west
		10 upper east
*/
rename hv024 region

* Type of place of residency
fre hv025
/*		1  urban
        2  rural
*/
rename hv025 urbanrural

* Ethnicity
fre v131 // (women; 14 < 50)
/*		1   akan
        2   ga/dangme
        3   ewe
        4   guan
        5   mole-dagbani
        6   grussi
        7   gruma
        8   mande
        996 other
*/
recode v131 (996=8)

fre mv131 // (men; 14 < 50)
/*		 1  akan
         2  ga/dangme
         3  ewe
         4  guan
         5  mole-dagbani
         6  grussi
         7  gruma
         8  hausa
         96 other
		 99
*/
recode mv131 (996=8) 

gen ethnicity = .
replace ethnicity = v131 if v131 < .
replace ethnicity = mv131 if mv131 < .
label var ethnicity "Ethnicity"
label def ethnicity							///
		 1  "Akan"							///
         2  "Ga/Adangbe"					///
         3  "Ewe"							///
         4  "Guan"							///
         5  "Mole-Dagbani"					///
         6  "Grussi"						///
         7  "Gruma"							///
         8 	"Other"	
label val ethnicity ethnicity

* Relationship to head
fre hv101
/*		1  head
        2  wife or husband
        3  son/daughter
        4  son/daughter-in-law
        5  grandchild
        6  parent
        7  parent-in-law
		8  brother/sister
		9  co-spouse
		10 other relative
		11 adopted/foster child
		12 not related
*/
rename hv101 relationship

* Usual resident
fre hv102
/*		0  no
        1  yes
*/
rename hv102 defacto

* Sex
fre hv104
/*		1  male
        2  female
*/
rename hv104 sex

* Age
fre hv105
/* 		Numerical
		97  97+
		98	dont know
*/
recode hv105 (98=.)
rename hv105 age

* Highest educational level 
fre hv106 
/*		0  no education, preschool
		1  primary
        2  secondary
        3  higher
        8  dont know
*/
recode hv106 (8=.)
rename hv106 educat

* Mother alive (only data for individuals < 15)
fre hv111
/*		0  no
        1  yes
		8  dont know
*/
recode hv111 (8 =.)
rename hv111 motheralive

* Mother's line number (only data for individuals < 15)
fre hv112 
/*		0  mother not in hh
        Numerical
*/
rename hv112 motherhhmid

* Father alive (only data for individuals < 15)
fre hv113
/*		0  no
        1  yes
		8  dont know
*/
recode hv113 (8 =.)
rename hv113 fatheralive

* Father's line number (only data for individuals < 15)
fre hv114 
/*	    0  father not in hh
        Numerical
*/
rename hv114 fatherhhmid

* Currently, formerly never married
fre hv116
/*		0  never married
		1  currently married (married + living together from hv115)
        2  formerly/ever married (widowed + divorced + not living together)
*/
rename hv116 gmaritalstatus
label def mstat 			///
	0 "Never married"		///
	1 "Cohabiting/married"	///
	2 "Formerly married"
label val gmaritalstatus mstat

* Currently working
fre v714 // (women 14 < 50)
/*		0  no
        1  yes
*/

fre mv714 // (men 14 < 60)
/*		0  no
        1  yes
*/

gen cwork = .
replace cwork = 0 if v714 == 0 | mv714 == 0
replace cwork = 1 if v714 == 1 | mv714 == 1
label var cwork "currently working"
label def cwork 0 "no" 1 "yes"
label val cwork cwork

* Wealth
fre hv270
/*		1   poorest
        2   poorer
        3   middle
        4   richer
        5   richest
*/
rename hv270 wealthindexquint
* Harmonise with labels for other waves
label def wealthindexquint			///
		1   "lowest quintile"		///
        2   "second quintile"		///
        3   "middle quintile"		///
        4   "fourth quintile"		///
        5   "highest quintile"		
label val wealthindexquint wealthindexquint	

* Religion
fre v130 // (women 14 < 50)
/*		   1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   pentecostal/charismatic
           6   other christian
           7   islam
           8   traditional/spiritualist
           9   no religion
          96   other

*/
fre mv130 // (men 14 < 60)
/*		   1   catholic
           2   anglican
           3   methodist
           4   presbyterian
           5   pentecostal/charismatic
           6   other christian
           7   islam
           8   traditional/spiritualist
           9   no religion
          96   other
*/
gen religion = .
replace religion = 1 if (v130 > 0 & v130 < 7) | (mv130 > 0 & mv130 < 7)
replace religion = 2 if (v130 == 7 | mv130 == 7)
replace religion = 3 if (v130 == 8 | mv130 == 8)
replace religion = 4 if v130 == 9 | v130 == 96 | mv130 == 9 | mv130 == 96
label var religion "Religion"
label def religion						///
	1 "Christian"						///
	2 "Muslim"							///
	3 "Traditional"						///
	4 "None/Other" 						
label val religion religion

* Consecutive household id variable
order wave v001 v002 v003
egen picktwo = tag(v001 v002)
drop hhid
gen hhid = sum(picktwo)

keep 																///
	wave v001 v002 v003	hhid wgt clstr_2014 stratum_2014			///	
	region urbanrural ethnicity										///
	relationship defacto	   										///
	sex age educat													///
	motheralive motherhhmid fatheralive fatherhhmid 				///
	gmaritalstatus													///
	cwork religion													///
	wealthindexquint
	

save "$temp/gdhs_2014", replace


/*------------------------------------------------------------------------------
 2018 (GSPS)
------------------------------------------------------------------------------*/

**#

/*------------------------------------------------------------------------------
 Replication of Wealth Index Creation
------------------------------------------------------------------------------*/
/* In the GDHS datasources, a wealth index is provided which was constructed 
using principal component analysis incorporating information on ownership of a 
range of goods such as cars and household items; dwelling characteristics such
as access to water and construction material; as well as agricultural land and 
real estate ownership (Ruthstein 2015: https://dhsprogram.com/programming/wealth
%20index/Steps_to_constructing_the_new_DHS_Wealth_Index.pdf). While no comparable 
wealth variable is provided for the 2018 GSPS wave, all of the individual 
variables used in the GDHS data to create the wealth index are available.
Therefore, following the detailed instructions for constructing the index in 
GDHS by Ruthstein (2015), I replicated the prinicpal component analysis for the 
GSPS datasource to obtain a similar wealh variable for the respective 2018 wave.
*/

* (i) Agricultural land data
use "$data_2018/04h_agsection.dta", clear // datastructure: plots within households
merge 1:1 FPrimary plotid using "$data_2018/04_plotroster.dta", nogen 
merge 1:1 FPrimary plotid using "$data_2018/04i_landtenure.dta", nogen

bysort FPrimary: gen n = _n

recode obtainhow				///
	(1 2 4 = 1 "owned") 		///
	(3 5 6 = 0 "not owned"), 	///
	gen(owned)
drop if owned == -666 

collapse (sum) totalsize, by(FPrimary)
save "$data_2018/land_owned.dta", replace

* (ii) Farm animal data
use "$data_2018/03ai_animalquestions.dta", clear // datastructure: animal types within households
encode animal, gen(animal_id)

gen drougtanimal = quantity if animal_id == 3
gen cattle = quantity if animal_id == 1
gen sheep = quantity if animal_id == 8
gen goats = quantity if animal_id == 5
gen pigs = quantity if animal_id == 6
gen rabbits = quantity if animal_id == 7
gen chicken =  quantity if animal_id == 2
gen other_animal = quantity if animal_id == 4

foreach x of varlist drougtanimal cattle sheep goats pigs rabbits chicken other_animal {
	bysort FPrimary: egen nr_`x' = max(`x')
	replace nr_`x' = 0 if nr_`x' == .
}

egen pickone = tag(FPrimary)
keep if pickone == 1
save "$data_2018/animals_owned.dta", replace


* (iii) Assets data
use "$data_2018/03aiii_durablegoodquestions.dta", clear // datastructure: assets within households
encode durablegood, gen(durablegood_id)

tab durablegood_id, gen(tool_)
foreach x of varlist tool* {
	bysort FPrimary: egen `x'_hh = max(`x')
	local label : variable label `x'
    label variable `x'_hh "`label'"
}

* Indicator variables for selected tools
rename tool_36_hh furniture
label variable furniture "Room furniture"

rename tool_37_hh sewmach
label variable furniture "Sewing machine"

rename tool_40_hh stovekero
label variable stovekero "Stove (kerosene)"

rename tool_38_hh stoveelectric
label variable stoveelectric "Stove (electric)"

rename tool_39_hh stovegas
label variable stovegas "Stove (gas)"

rename tool_35_hh refrigerator
label variable refrigerator "Refrigerator"

rename tool_18_hh freezer
label variable freezer "Freezer"

rename tool_2_hh airc
label variable airc "Air conditioner"

rename tool_17_hh fan
label variable fan "Fan"

rename tool_33_hh radio
label variable radio "Radio"

gen computer = 0
replace computer = 1 if tool_15_hh == 1 | tool_25_hh == 1
label variable computer "Computer"

gen camera = 0
replace camera = 1 if tool_7_hh == 1 | tool_8_hh == 1
label variable camera "Camera"

rename tool_21_hh iron
label variable iron "Iron"

rename tool_3_hh bicycle
label variable bicycle "Bicycle"

rename tool_28_hh motorcycle
label variable motorcycle "Motorcycle"

rename tool_5_hh books
label variable books "Textbooks"

rename tool_11_hh mobilephone
label variable mobilephone "Mobile phone"

rename tool_42_hh fixedlinephone
label variable fixedlinephone "Fixed line telephone"

rename tool_41_hh television
label variable television "TV"

rename tool_49_hh washingmach
label variable washingmach "Washing machine"

rename tool_24_hh gaslights
label variable gaslights "Lanterns / gas lights"

rename tool_9_hh car
label variable car "Car"

rename tool_45_hh truck
label variable truck "Truck / bus"

gen nonagri_landownership = 0
replace nonagri_landownership = 1 if tool_22_hh == 1 | tool_23_hh == 1
label variable nonagri_landownership "Land ownership (non-farm)"

egen pickone = tag(FPrimary)
keep if pickone == 1
save "$data_2018/assets.dta", replace

* (iv) Open householdmember dataset which serves as master file
use "$data_2018/01b2_roster", clear 

* Urban/rural variable
merge m:1 FPrimary using "$data_2018/key_hhld_info_EDIT.dta", keep(1 3) nogen // from wave 1
merge m:1 FPrimary using "$data_2018/commtype.dta", keep(1 3) nogen

fre communitytype
fre urbrur
/*		1  urban
        2  rural
*/
tab urbrur communitytype, m // missings & contradictory data -> maybe leave out
clonevar urbanrural = communitytype
replace urbanrural = urbrur if communitytype == . & urbrur < .
label var urbanrural "Type of place of residence"


* (1) Domestic servant
gen domestic_servant_aux = (relationship == 9)
bysort FPrimary: egen domestic_servant = max(domestic_servant_aux) 

* (2) (Agricultural) land ownership
merge m:1 FPrimary using "$data_2018/land_owned.dta", nogen 
gen landownership =.
replace landownership = 1 if totalsize > 0 & totalsize < .
replace landownership = 0 if totalsize == 0 | totalsize == .

* (3) Home ownership
merge m:1 FPrimary using "$data_2018/12b_housingcharacteristics2.dta", nogen 
recode presentstatus				///
	(1 = 1 "ownership")				///
	(2 / 4 = 0 "no ownership"), 	///
	gen(homeownership)

* (4) Source of drinking water
merge m:1 FPrimary using "$data_2018/12a_housingcharacteristics1ii_utilities.dta", nogen
recode drinkingwatersource								///
	(1 = 1 "Indoor plumbing")				///
	(2 = 2 "Inside standpipe")				///
	(3 = 3 "Water truck")					///
	(4 = 4 "Water vendor")					///
	(5 = 5 "Pipe neighbours")				///
	(6 = 6 "Private outside standpipe")		///
	(7 = 7 "Public standpipe")				///
	(8 = 8 "Sachet/bottled water")			///
	(9 = 9 "Barehole")						///
	(10 = 10 "Protected well")				///
	(11 = 11 "Unprotected well")			///
	(12 / 14 = 12 "Natural surface source") ///
	(15 = .), 								///
	gen(watersource)
	
* improved indicator
recode drinkingwatersource						///
	(1 / 10  = 1 "improved")		///
	(11 / 14 = 0 "unimproved")		///
	(-666 = .),						///
	gen(watersource_improved)

* Indicator variables
tab watersource, gen(watersource_)

* (6) Toilet type
merge m:1 FPrimary using "$data_2018/12a_housingcharacteristics1iii_structure.dta", nogen
rename mobilephone accessmobile

tab toilettype, gen(toilettype_)

* (un)improved indicator
recode toilettype						///
	(1/3  	  = 1 "improved")			///
	(0 4 / 6	  = 0 "unimproved")		///
	(-777 -666 = .),					///					
	gen(toilet_improved)

* (7) Wall material
tab materialouterwall, gen(wallmat_)

* improved indicator
recode materialouterwall							///
	(4/7  = 1 "improved")				///
	(-666 1/3 8/11 = 0 "unimproved"),		///					
	gen(wall_improved)	
	
* (7) Floor material
tab materialfloor, gen(floormat_)

* (un)improved indicator
recode materialfloor							///
	(2/8 = 1 "improved")				///
	(-666 1 = 0 "unimproved"),		///					
	gen(floor_improved)
	
* (8) Roof material
tab materialroof, gen(roofmat_)
	
* (un)improved indicator
recode materialroof						///
	(2/6   = 1 "improved")				///
	(-666 1 7/8 = 0 "unimproved"),			///					
	gen(roof_improved)
	
* (9) Cooking fuel
tab cookingfuel, gen(cookingfuel_)

* clean indicator
recode cookingfuel						///
	(4/6      = 1 "clean")				///
	(-666 1/3 7  = 0 "solid"),				///					
	gen(fuel_clean)	
	
* (10) Members per sleeping room
bysort FPrimary: gen N = _N
recode bedrooms (0 = 1) // set 0 bedrooms to 1 bedroom
gen bedroomdensity = N/bedrooms // household members / number of bedrooms

* (11) Farm animals
merge m:1 FPrimary using "$data_2018/animals_owned.dta", nogen 

foreach x in drougtanimal cattle sheep goats pigs rabbits chicken other_animal {
    recode nr_`x'				///
	(0 . = 0 "no `x'")			///
	(1/max = 1 "owns `x'"), 	///
	gen(`x'_ownership)
	label var `x'_ownership "`x' ownership"
}

* (12) Electricity
rename electricity electricity_old
recode electricity_old				///
	(1 = 1 "Yes")			///
	(5 = 0 "No"),			///
	gen(electricity)

* (13) Assets
merge m:1 FPrimary using "$data_2018/assets.dta", nogen 


* (v)  Keep only one row per household
distinct FPrimary
egen pickhh = tag (FPrimary)
keep if pickhh == 1


/*------------------------------------------------------------------------------
III PCA
------------------------------------------------------------------------------*/

* (i) Create list with PCA variables
vl create pcavars = (													///		
	domestic_servant landownership nonagri_landownership homeownership 	///
	watersource_improved toilet_improved wall_improved					///
	floor_improved roof_improved fuel_clean								///
	bedroomdensity electricity drougtanimal_ownership cattle_ownership	///
	sheep_ownership goats_ownership	pigs_ownership rabbits_ownership	///
	chicken_ownership other_animal_ownership							///
	furniture sewmach stovekero stoveelectric stovegas 					///
	refrigerator freezer airc fan radio iron bicycle motorcycle books 	///
	mobilephone fixedlinephone television washingmach gaslights car 	///
	truck computer camera)

* (ii) Examine range and variance in PCA ariables
sum	$pcavars

* (iii) Common factor analysis (urban and rural households combined)
pca $pcavars
estat kmo 									// check sampling adequacy
predict combscores 							// store factors
hist combscores 							// examine distribution

* (iv) Factor analysis for urban households
pca $pcavars if urbanrural == 1										
estat kmo 									// check sampling adequacy
predict urb1 								// store factors
gen urbscores = urb1 if urbanrural == 1		// store urban factors

* (v) Rural analysis for rural households
pca $pcavars if urbanrural == 2										
estat kmo 									// check sampling adequacy
predict rur1 								// store factors
gen rurscores = rur1 if urbanrural == 2		// store rural factors

* (vi) Regress combined scores on urban and rural scores to form final scores
gen finscores = .
foreach var in urbscores rurscores {
	reg combscores `var'
	local constant_`var' _b[_cons]
	local coef_`var' _b[`var']
	replace finscores = `constant_`var'' + `var' * `coef_`var'' if `var' < .
}

* (vii) Generate quintiles (final weealth index) 
xtile wealthindexquint = finscores, nq(5)
label def wealthindexquint			///
		1   "lowest quintile"		///
        2   "second quintile"		///
        3   "middle quintile"		///
        4   "fourth quintile"		///
        5   "highest quintile"		
label val wealthindexquint wealthindexquint	
label var wealthindexquint "Wealth index (quintiles)"

* Keep only relevant variables and save dataset
keep FPrimary wealthindexquint

save "$data_2018/wealth.dta", replace


/*------------------------------------------------------------------------------
 Data Harmonization with GDHS
------------------------------------------------------------------------------*/

**#

* MERGING DATASETS

* Open householdmember dataset which serves as master data
use "$data_2018/01b2_roster.dta", clear 

* Merge other relevant datasets
merge 1:1 FPrimary hhmid using "$data_2018/01d_background.dta", nogen
merge m:1 FPrimary using "$data_2018/00_hh_info.dta", nogen 

* Wave identifier
gen wave = 2018

* Household number
clonevar v002 = FPrimary
destring v002, replace 

* Respondent's line number
clonevar v003 = hhmid

* (i) Household weight
* not avaiable --> generate own post-stratification weights

/* Population statistics used as basis for calculation: 
	Ghana 2021 Population and Housing Census (Ghana Statistica Service 2021, 
		see reference in main thesis, Table 5.18 (p. 76))
* Note: Six new regions were created in 2018, so that regions in census table 
		differ from regions in GSPS dataset, but can be harmonized
*/

egen pickone = tag(FPrimary)
fre region if pickone == 1

/** Census 2021: Households per Region

					 |   Freq.(C)    Percent(C)  Percent(GSPS)     Weight     
---------------------+---------------------------------------------------
Ashanti Region       |  1,523,101      	  18.21     	 17.41   1.045951
Brong Ahafo Region   |    759,520      	   9.08      	  9.61   0.944849
	Bono			 |    317,994
	Bono East		 |    288,725
	Ahafo			 |    152,801
Central Region       |    838,493         10.02    		  8.20   1.221951
Eastern Region       |    881,328     	  10.54      	 11.48	 0.918118
Greater Accra Region |  1,702,160         20.35          11.41   1.783523
Northern Region      |    679,136      	   8.12			 14.09	 0.576295
	Northern		 |	  437,934
	Savannah		 |	  133,114
	North East		 |	  108,088
Upper East Region    |    264,404          3.16      	  5.34	 0.591760
Upper West Region    |    190,193          2.27     	  3.92	 0.579082
Volta Region         |    665,404   	   7.95 		  9.49	 0.837724
	Volta			 |	  491,373
	Oti				 |	  174,031
Western Region       |    861,435   	  10.30			  9.03 	 1.140642
	Western			 |	  621,349 
	Western North	 |	  240,086
---------------------+---------------------------------------------------
Total                |  8,365,174        100.00       

*/
* Note: (C): Data copied from census table

* Calculation of weights = percent(C) / Percent(GSPS)
* e.g. for Ashanti: wgt= 18.21 / 17.41 = 1.045951

* (i) Household weight
encode region, gen(region2)
drop region
rename region2 region

gen wgt = .
replace wgt = 1.045951 if region == 1
replace wgt = 0.944849 if region == 2
replace wgt = 1.221951 if region == 3
replace wgt = 0.918118 if region == 4
replace wgt = 1.783523 if region == 5
replace wgt = 0.576295 if region == 6
replace wgt = 0.591760 if region == 7
replace wgt = 0.579082 if region == 8
replace wgt = 0.837724 if region == 9
replace wgt = 1.140642 if region == 10
label var wgt "Household Weight"

* (ii) Cluster (PSU)
* no cluster variable

* (iii) Stratum (could also be made 20 with info from data and census)
clonevar stratum_2018 = region // 10 strata
label var stratum_2018 "Stratum"


* SOCIODEMOGRAPHIC VARIABLES

* Region
fre region
/*		1	Ashanti Region
        2   Brong Ahafo Region
        3   Central Region
        4   Eastern Region
        5   Greater Accra Region
        6   Northern Region
        7   Upper East Region
        8   Upper West Region
        9   Volta Region
       10   Western Region
*/

* Harmonize with GDHS coding:
rename region region_old
recode region_old 					///
	(1 = 6 "Ashanti")				///
	(2 = 7 "Brong-Afaho")			///
	(3 = 2 "Central")				///
	(4 = 5 "Eastern")				///
	(5 = 3 "Greater Accra")			///
	(6 = 8 "Northern")				///
	(7 = 10 "Upper East")			///
	(8 = 9 "Upper West")			///
	(9 = 4 "Volta")					///
	(10 = 1 "Western"),				///
	gen(region)
label var region "Region"


* Type of place of residency
merge m:1 FPrimary using "$data_2018/key_hhld_info_EDIT.dta", keep(1 3) nogen 
* Note: copied from wave 1 (initial household info)
merge m:1 FPrimary using "$data_2018/commtype.dta", keep(1 3) nogen

* Type of place of residency
fre communitytype
fre urbrur
/*		1  urban
        2  rural
*/
tab urbrur communitytype, m // missings & contradictory data -> maybe leave out
clonevar urbanrural = communitytype
replace urbanrural = urbrur if communitytype == . & urbrur < .
label var urbanrural "Type of place of residence"

* Ethnicity
fre ethnicity // many sub-ethnicites 

* harmonize with GDHS (see GSPS codebook for meta-ethnicities)
rename ethnicity ethnicity_old
gen ethnicity = .
replace ethnicity = 1 if ethnicity_old > 0 & ethnicity_old < 19
replace ethnicity = 2 if ethnicity_old > 19 & ethnicity_old < 23
replace ethnicity = 3 if ethnicity_old == 30
replace ethnicity = 4 if ethnicity_old > 40 & ethnicity_old < 48
replace ethnicity = 5 if ethnicity_old > 60 & ethnicity_old < 70
replace ethnicity = 6 if ethnicity_old > 70 & ethnicity_old < 76
replace ethnicity = 7 if ethnicity_old > 50 & ethnicity_old < 58
replace ethnicity = 8 if ethnicity_old < 0
replace ethnicity = 8 if ethnicity_old > 80 & ethnicity_old < .

label var ethnicity "Ethnicity"
label drop ethnicity
label def ethnicity							///
		 1  "Akan"							///
         2  "Ga/Adangbe"					///
         3  "Ewe"							///
         4  "Guan"							///
         5  "Mole-Dagbani"					///
         6  "Grussi"						///
         7  "Gruma"							///
         8 	"Other"	
label val ethnicity ethnicity

* Relationship to head
fre relationship
/*		 1   Head
         2   Spouse
         3   Child
         4   Grandchild
         5   Parent/Parent-in-law
         6   Son/Daughter-in-law
         7   Other relative
         8   Adopted, foster/stepchild
         9   Househelp
        10   Non-relative
*/

* Harmonize with GDHS coding: 
rename relationship relationship_old
recode relationship_old 			///
	(1 = 1 "head")					///
	(2 = 2 "wife or husband")		///
	(3 = 3 "son/daughter")			///
	(4 = 5 "grandchild")			///
	(5 = 6 "parent")				///
	(6 = 4 "son/daughter-in-law")	///
	(7 = 10 "other relative")		///
	(8 = 11 "adopted/foster child")	///
	(9 10 = 12 "not related"),		///
	gen(relationship)
label var relationship "Relationship"

* Usual resident
* no variable in GSPS

* Sex
fre gender
/*		1  Male
        5  Female
*/
* Harmonize with GDHS coding:
recode gender (1=1 "male") (5 = 2 "female"), gen(sex)
label variable sex "Sex"

* Age
fre age // Numerical
* Harmonize with GDHS coding:
rename age age_old
recode age_old (97/111 = 97 "97+"), gen(age)
label var age "Age"

* Highest educational level 
merge 1:1 FPrimary hhmid using "$data_2018/01gi_generaleducation.dta", nogen
fre attended
/*		1  Yes
        5  No
*/
fre highestgrade
/* 		-999   Don't know
        -888   Refuse to answer
        -666   Other (please specify)
           0   None
           1   Pre-school
          11   P1
          12   P2
          13   P3
          14   P4
          15   P5
          16   P6
          17   JSS1
          18   JSS2
          19   JSS3
          20   M1
          21   M2
          22   M3
          23   M4
          24   SSS1
          25   SSS2
          26   SSS3
          27   SSS4
          28   S1
          29   S2
          30   S3
          31   S4
          32   S5
          33   L6
          34   U6
*/
tab highestgrade attended if age < 18, m
* Harmonize with GDHS coding:
gen educat = .
replace educat = 0 if attended == 5
replace educat = . if highestgrade < 0
replace educat = 0 if highestgrade == 0
replace educat = 1 if highestgrade > 0 & highestgrade < 17
replace educat = 2 if highestgrade > 16 & highestgrade < 28
replace educat = 3 if highestgrade > 27 & highestgrade < .
label var educat "Education level"
label def educat 							///
	0 "no education, preschool"				///
	1 "primary"								///
	2 "secondary"							///
	3 "higher"
label val educat educat


* Mother alive (only data for individuals < 15)
fre motherinhouse
/*		1  Yes
        3  No, he/she is deceased
		5  No, he/she lives in another household
*/
* Harmonize with GDHS coding:
recode motherinhouse (1 5 = 1 "yes") (3 = 0 "no"), gen(motheralive)
label var motheralive "Mother alive"

* Mother's line number
fre motherid // Numerical
* Harmonize with GDHS coding:
rename motherid motherhhmid
replace motherhhmid = 0 if motherinhouse == 3 | motherinhouse == 5
label def motherhhmid 0 "mother not in household"
label val motherhhmid motherhhmid

* Father alive (only data for individuals < 15)
fre fatherinhouse
/*		1  Yes
        3  No, he/she is deceased
		5  No, he/she lives in another household
*/
* Harmonize with GDHS coding:
recode fatherinhouse (1 5 = 1 "yes") (3 = 0 "no"), gen(fatheralive)
label var fatheralive "Father alive"

* Father's line number (only data for individuals < 15)
fre fatherid // Numerical
* Harmonize with GDHS coding:
rename fatherid fatherhhmid
replace fatherhhmid = 0 if fatherinhouse == 3 | fatherinhouse == 5
label def fatherhhmid 0 "father not in household"
label val fatherhhmid fatherhhmid

* Currently, formerly never married
fre maritalstatus
/*		1   Married
        2   Consensual union
        3   Separated
        4   Divorced
        5   Widowed
        6   Never married
        7   Betrothed
*/
* Harmonize with GDHS coding:
recode maritalstatus 							///
	(6 = 0 	   "Never married")					///
	(1 2 7 = 1 "Cohabiting/married")			///
	(3 4 5 = 2 "Formerly married"),				///
	gen(gmaritalstatus)
label var gmaritalstatus "Marital status"

* Currently working
merge 1:1 FPrimary hhmid using "$data_2018/01fa_employmentscreener.dta", nogen
fre paidemployed 
/*		1  yes
        5  no
*/
fre businessowner
/*		1  yes
        5  no
*/
fre businesscontributor
/*		1  yes
        5  no
*/
fre farmcontributor
/*		1  yes
        5  no
*/
* * Harmonize with GDHS coding (approximatevly:
gen cwork = .
replace cwork = 0 if paidemployed == 5 & businessowner == 5 ///
	& businesscontributor == 5 & farmcontributor == 5
replace cwork = 1 if paidemployed == 1 | businessowner == 1 ///
	| businesscontributor == 1 | farmcontributor == 1
label var cwork "currently working"
label def cwork 0 "no" 1 "yes"
label val cwork cwork

* Wealth
merge m:1 FPrimary using "$data_2018/wealth.dta", nogen

fre wealthindexquint
/*		1   lowest quintile
        2   second quintile
        3   middle quintile
        4   fourth quintile
        5   highest quintile

*/

* Religion
fre religion 
/*		-999   Don't know
        -888   Refuse to answer
        -666   Other (please specify)
           0   None
           1   Christian
           2   Muslim
           3   Traditional
*/
rename religion religion_old
label drop religion
gen religion = .
replace religion = 1 if religion_old == 1
replace religion = 2 if religion_old == 2
replace religion = 3 if religion_old == 3
replace religion = 4 if religion_old == 0 | religion_old == -666
label var religion "Religion"
label def religion						///
	1 "Christian"						///
	2 "Muslim"							///
	3 "Traditional"						///
	4 "None/Other" 						
label val religion religion

* Consecutive household id variable
egen picktwo = tag(FPrimary)
drop hhid
gen hhid = sum(picktwo)

keep 																///
	wave v002 v003 hhid wgt stratum_2018							///	
	region urbanrural ethnicity										///
	relationship	   												///
	sex age educat 													///
	motheralive motherhhmid fatheralive fatherhhmid 				///
	gmaritalstatus													///
	cwork religion													///
	wealthindexquint
	

save "$temp/gsps_2018", replace

/*------------------------------------------------------------------------------
III Appending and Further Variable Preparation
------------------------------------------------------------------------------*/

**#

/*------------------------------------------------------------------------------
 Appending 1993 - 2014 Waves
------------------------------------------------------------------------------*/

use "$temp/gdhs_1993", clear
append using "$temp/gdhs_1998"
append using "$temp/gdhs_2003"
append using "$temp/gdhs_2008"
append using "$temp/gdhs_2014"
append using "$temp/gsps_2018"

/*------------------------------------------------------------------------------
 Weight Variables
------------------------------------------------------------------------------*/

* Cluster Variable with distinct values for every wave ("super-cluster")
*	only for DHS datasets (1993 - 2014)
fre clstr*
gen clstr_ID = .
replace clstr_ID = clstr_1993 if clstr_1993 < .
replace clstr_ID = clstr_1998 + 500 if clstr_1998 < .
replace clstr_ID = clstr_2003 + 1000 if clstr_2003 < .
replace clstr_ID = clstr_2008 + 1500 if clstr_2008 < .
replace clstr_ID = clstr_2014 + 2000 if clstr_2014 < .
label var clstr_ID "Cluster ID"

* Stratum Variable with distinct values for every wave ("super-stratum")
*	only for DHS datasets (1993 - 2014)
fre stratum*
gen stratum_ID = .
replace stratum_ID = stratum_1993 if stratum_1993 < .
replace stratum_ID = stratum_1998 + 20 if stratum_1998 < .
replace stratum_ID = stratum_2003 + 40 if stratum_2003 < .
replace stratum_ID = stratum_2008 + 60 if stratum_2008 < .
replace stratum_ID = stratum_2014 + 80 if stratum_2014 < .
label var stratum_ID "Stratum ID"

/*------------------------------------------------------------------------------
 Consecutive Household ID and Individual ID Variables
------------------------------------------------------------------------------*/

* Household ID
egen picktwo = tag(wave hhid)
drop hhid
gen hhid = sum(picktwo)
label var hhid "Household ID"
drop picktwo

* Household Pick Variable
egen hhpickone = tag(hhid)
label var hhpickone "Household Identificator"

* Household Member ID 
rename v003 hhmid
label var hhmid "Household Member ID"

* Individual ID
egen picktwo = tag(hhid hhmid)
gen pid = sum(picktwo)
label var pid "Individual ID"
drop picktwo


/*------------------------------------------------------------------------------
 Household Size, Child Indicator and Number of Children Variables
------------------------------------------------------------------------------*/

* First Sample Restriction
distinct hhid
* Drop non-permanent household members (visitors)
drop if defacto == 0 
* Drop households comprising individuals with missing on relationship variable
list hhid if relationship == .
drop if hhid == 12063 | hhid == 12189 | hhid == 14857 | hhid == 16764
distinct hhid // 1866 individuals and 81 households lost

* Household Size
bysort hhid: gen hh_size = _N
fre hh_size if hhpickone == 1
label var hh_size "Household Size"

* Child Indicator (Def: Below Age 18)
gen childind = .
replace childind = 0 if age > 17 & age < .
replace childind = 1 if age < 18
label var childind "Child indicator (< 18)"
label def childind				///
		0 "No child (>= 18)"	///
		1 "Child (< 18)"
label val childind childind

* Alternative Child Indicator (Def: Below Age 15)
gen childind2 = .
replace childind2 = 0 if age > 14 & age < .
replace childind2 = 1 if age < 15
label var childind2 "Child indicator (< 15)"
label def childind2				///
		0 "No child (>= 15)"	///
		1 "Child (< 15)"
label val childind2 childind2

* Number of Children in Household
bysort hhid: gen aux = sum(childind)
bysort hhid: egen hh_nr_children = max(aux)
label var hh_nr_children "Number of children in Household"

/*------------------------------------------------------------------------------
 Household Constellation Variables
------------------------------------------------------------------------------*/

**#

* Relationship indicator (extended to all household members)
cap drop i*
tab relationship, gen(i)
foreach x of varlist i* {
	 bysort hhid: egen `x'h = max(`x') 
	 }

* Number of relationships of certain type within household (extended to all
* household members) 
foreach x of varlist i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 {
	 bysort hhid: egen `x'nr = sum(`x') 
	 }	

* (1) Nuclear Indicator
/* Def Nuclear: only parent(s) with their biological or adopted/foster children
and no other (non-)relatives */
* including polygynous nuclear families (one anchor + > 1 wives/co-wives)
* including children living with siblings only -> Case C
* including children living alone or only with spouse -> Case D

cap drop nuclear
gen nuclear = .

* Case A: anchor person is parent (1st generation in 2 generation households)
replace nuclear = 1 if 														 ///
	(i1h == 1 | i2h == 1 | i9h == 1) & 										 ///
	(i3h == 1 | i11h == 1) &												 ///
	 i4h == 0 &  i5h == 0  & 												 ///
	 i6h == 0 &  i7h == 0  & 												 ///
	 i8h == 0 &  i10h == 0 & i12h == 0
* (household head OR wife/husband OR co-spouse) &
* (son/daughter OR adopted/foster child) & 
* NO son/daughter-in-law & NO grandchild &
* NO parent & NO parent-in-law &
* NO brother/sister & NO other relative & NO non-relative

* Case B: anchor person is child (2nd generation in 2 generation households)
replace nuclear = 1 if														 /// 
	i1h == 1 & i2h == 0 & i9h == 0 &									     ///
	i3h == 0 & i4h == 0 & i5h == 0 & i6h == 1 &								 ///
	i7h == 0 & (i8h == 0 | i8h == 1) & i10h == 0 & 							 ///
	i11h == 0 & i12h == 0
* household head & NO wife/husband & NO co-spouse &
* NO son/daughter & NO son/daughter-in-law & NO grandchild & parent &
* NO parent-in-law & (NO) brother/sister & NO other relative & 
* NO adopted/foster child & NO non-relative

* Case C: anchor person is sibling (only anchor with brother/sister)
replace nuclear = 2 if														 ///
	(i1h == 0 | i1h == 1) & i2h == 0 & i9h == 0 &							 ///
	i3h == 0 & i4h == 0 & i5h == 0 & i6h == 0 &								 ///
	i7h == 0 & i8h == 1 & i10h == 0 & 							 			 ///
	i11h == 0 & i12h == 0 
* (NO) household head & & NO wife/husband & NO co-spouse &
* NO son/daughter & NO son/daughter-in-law & NO grandchild & NO parent &
* NO parent-in-law & brother/sister & NO other relative & 
* NO adopted/foster child & NO non-relative

* Case D: one-generation household (child alone or child with (co-)spouse only)
replace nuclear = 3 if														 ///
	(i1h == 0 |  i1h == 1) & (i2h == 0 | i2h == 1) & (i9h == 0 | i9h == 1) & ///
	 i3h == 0 & i4h == 0 & i5h == 0 & i6h == 0 &							 ///
	 i7h == 0 & i8h == 0 & i10h == 0 & 							 			 ///
	 i11h == 0 & i12h == 0 
* (NO) household head & (NO) wife/husband & (NO) co-spouse &
* NO son/daughter & NO son/daughter-in-law & NO grandchild & NO parent &
* NO parent-in-law & NO brother/sister & NO other relative &	
* NO adopted/foster child & NO non-relative	

label var nuclear "Nuclear"
label def nuclear 						///
	1 "Parent-children"					///
	2 "Siblings"						///
	3 "Single or with spouse"
label val nuclear nuclear


* (2) Shared Indicator
/* Def Shared / Doubled-Up: biological or adopted/foster children with (no) 
parent(s) and at least one non-relative or extended relative */

cap drop shared
gen shared = .

* Case A: anchor person is parent (2nd generation 3 generation households)
replace shared = 1 if														 ///
	(i1h == 0 | i1h == 1) & (i2h == 0 | i2h == 1) & (i9h == 0 | i9h == 1) &  ///
	(i3h == 1 | i11h == 1) & i5h == 0 &										 ///
	(i4h == 1 | i6h == 1 | i7h == 1 | i8h == 1 | i10h == 1 | i12h == 1)
* (NO) household head & (NO) wife/husband & (NO) co-spouse &
* (son/daughter OR adopted/foster child) & NO grandchild &
* (son-daughter-in-law OR parent OR parent-in-law OR other relative OR
* non-relative)

* Case B: anchor person is grandparent (1st generation in 3 generation households)
replace shared = 1 if														 ///
	(i1h == 1 | i2h == 1 | i9h == 1) & 										 ///
	 i5h == 1 & (i3h == 0 | i3h == 1) & (i4h == 0 | i4h == 1) &				 ///
	(i6h == 0 | i6h == 1) & (i7h == 0 | i7h == 1) & (i8h == 0 | i8h == 1) &  ///
	(i10h == 0 | i10h == 1) & (i11h == 0 | i11h == 1) & (i12h == 0 | i12h == 1)
* (household head OR wife/husband OR co-spouse) &
* grandchild & (NO) son/daughter & (NO) son/daughter-in-law
* (NO) parent & (NO) parent-in-law & (NO) brother/sister & 
* (NO) other relative & (NO) adopted/foster child & (NO) non-relative

* Case C: anchor person is neither parent nor grandparent
replace shared = 1 if														 ///
	(i1h == 0 | i1h == 1) & (i2h == 0 | i2h == 1) & (i9h == 0 | i9h == 1) &  ///
	(i4 == 1 | i10 == 1 | i12 == 1)	&										 ///
	(i3h == 0 | i3h == 1) & (i5h == 0 | i5h == 1) & (i6h == 0 | i6h == 1) &  ///
	(i7h == 0 | i7h == 1) & (i8h == 0 | i8h == 1) & (i11h == 0 | i11h == 1)	
* (NO) household head & (NO) wife/husband & (NO) co-spouse &
* IS (son/daughter-in-law OR other relative OR non-relative) & 
* (NO) son/daughter & (NO) grandchild & (NO) parent & 
* (NO) parent-in-law & (NO) brother/sister & (NO) adopted/foster child

* Case D: anchor person is sibling
replace shared = 1 if														 ///
	(i1h == 0 | i1h == 1) & i8 == 1 & (i6h == 0 | i6h == 1)	&			 	 ///
	(i2h == 1 | i9h == 1 | i3h == 1 | i4h == 1 |							 ///
	 i5h == 1 | i7h == 1 | i10h == 1 | i11h == 1 |							 ///
	 i12h == 1)
* (NO) household head & IS brother/sister & (NO) parent &
* (wife/husband OR co-spouse OR son/daughter OR son/daughter-in-law OR
* grandchild OR parent-in-law OR other relative OR adopted/foster child OR
* non-relative

label var shared "Shared Indicator"
label def shared				///
	1 "Shared"
label val shared shared


* (3) Extended Indicator
/* Def Extended: biological or adopted/foster children with (no) 
parent(s) and at least one extended relative */
* irrespective of whether also non-relatives reside
* -> subgroup of shared/doubled up

cap drop extended
gen extended = .

* Case A: anchor person is parent (2nd generation 3 generation households)
replace extended = 1 if														 ///
	(i1h == 0 | i1h == 1) & (i2h == 0 | i2h == 1) & (i9h == 0 | i9h == 1) &  ///
	(i3h == 1 | i11h == 1) & i5h == 0 &										 ///
	(i4h == 1 | i6h == 1 | i7h == 1 | i8h == 1 | i10h == 1) & 				 ///
	(i12h == 0 | i12h == 1)
* (NO) household head & (NO) wife/husband & (NO) co-spouse &
* (son/daughter OR adopted/foster child) & NO grandchild &
* (son-daughter-in-law OR parent OR parent-in-law OR other relative) &
* (NO) non-relative

* Case B: anchor person is grandparent (1st generation in 3 generation households)
replace extended = 1 if														 ///
	(i1h == 1 | i2h == 1 | i9h == 1) & 										 ///
	 i5h == 1 & (i3h == 0 | i3h == 1) & (i4h == 0 | i4h == 1) &				 ///
	(i6h == 0 | i6h == 1) & (i7h == 0 | i7h == 1) & (i8h == 0 | i8h == 1) &  ///
	(i10h == 0 | i10h == 1) & (i11h == 0 | i11h == 1) & (i12h == 0 | i12h == 1)
* (household head OR wife/husband OR co-spouse) &
* grandchild & (NO) son/daughter & (NO) son/daughter-in-law
* (NO) parent & (NO) parent-in-law & (NO) brother/sister & 
* (NO) other relative & (NO) adopted/foster child & (NO) non-relative

* Case C: anchor person is neither parent nor grandparent
replace extended = 1 if														 ///
	(i1h == 0 | i1h == 1) & (i2h == 0 | i2h == 1) & (i9h == 0 | i9h == 1) &  ///
	(i4 == 1 | i10 == 1) &										 			 ///
	(i3h == 0 | i3h == 1) & (i5h == 0 | i5h == 1) & (i6h == 0 | i6h == 1) &  ///
	(i7h == 0 | i7h == 1) & (i8h == 0 | i8h == 1) & 						 ///
	(i11h == 0 | i11h == 1)	& (i12h == 0 | i12h == 1) 
* (NO) household head & (NO) wife/husband & (NO) co-spouse &
* IS (son/daughter-in-law OR other relative) & 
* (NO) son/daughter & (NO) grandchild & (NO) parent & 
* (NO) parent-in-law & (NO) brother/sister &
* (NO) adopted/foster child & (NO) non-relative

* Case D: anchor person is sibling
replace extended = 1 if														 ///
	(i1h == 0 | i1h == 1) & i8 == 1 & (i6h == 0 | i6h == 1)	&			 	 ///
	(i2h == 1 | i9h == 1 | i3h == 1 | i4h == 1 |							 ///
	 i5h == 1 | i7h == 1 | i10h == 1 | i11h == 1 ) &	  					 ///
	(i12h == 0 | i12h == 1)
* (NO) household head & IS brother/sister & (NO) parent &
* (wife/husband OR co-spouse OR son/daughter OR son/daughter-in-law OR
* grandchild OR parent-in-law OR other relative OR adopted/foster child) &
* (NO) non-relative

label var extended "Extended Indicator"
label def extended				///
	1 "Extended"
label val extended extended


* (3) Grandparents Indicator
/* Def Grandparents: biological or adopted/foster children with (no) 
parent(s) and at least one grandparent and (no) other (non-) relatives */
* -> subgroup of shared/doubled up

cap drop grandparent
gen grandparent = .

* Case A: anchor person is parent (2nd generation 3 generation households)
replace grandparent = 1 if													 ///
	(i1h == 0 | i1h == 1) & (i2h == 0 | i2h == 1) & (i9h == 0 | i9h == 1) &  ///
	(i3 == 1 | i11 == 1) & i5h == 0 &										 ///
	(i6h == 1 | i7h == 1) &													 ///
	(i4h == 0 | i4h == 1) & (i8h == 0 | i8h == 1) &							 ///
	(i10h == 0 | i10h == 1) & (i12h == 0 | i12h == 1)
* (NO) household head & (NO) wife/husband & (NO) co-spouse &
* IS (son/daughter OR adopted/foster child) & NO grandchild &
* (parent OR parent-in-law) &
* (NO) son/daughter-in-law & (NO) brother/sister &
* (NO) other relative & (NO) non-relative

* Case B: anchor person is grandparent (1st generation in 3 generation households)
replace grandparent = 1 if													 ///
	(i1h == 1 | i2h == 1 | i9h == 1) & 										 ///
	i5 == 1 & (i3h == 0 | i3h == 1) & (i4h == 0 | i4h == 1) &				 ///
	(i6h == 0 | i6h == 1) & (i7h == 0 | i7h == 1) & (i8h == 0 | i8h == 1) &	 ///
	(i10h == 0 | i10h == 1) & (i11h == 0 | i11h == 1) & (i12h == 0 | i12h == 1)
* (household head OR wife/husband OR co-spouse) &
* IS grandchild & (NO) son/daughter & (NO) son/daughter-in-law &
* (NO) parent & (NO) parent-in-law & (NO) brother/sister &
* (NO) other relative & (NO) adopted/foster child & (NO) non-relative 

label var grandparent "Grandparent Indicator"
label def grandparent ///
	1 "w/ Grandparent"	
label val grandparent grandparent

* Skipped generation vs multigenerational households (only children < 15)
bysort wave: tab fatherhhmid motherhhmid if age < 15 & grandparent == 1, m
tab fatheralive motheralive if age < 15 & grandparent == 1, m


* (4) Manual recoding of special cases not captured by algorithms

tab nuclear shared if childind == 1, m // 39 children not identified
cap drop aux
cap drop aux2
gen aux = 1 if nuclear == . & shared == . & childind == 1
bysort hhid: egen aux2 = max(aux)
order wave hhid hhmid pid shared extended grandparent age

replace nuclear = 1 if inlist(pid, 77926, 78056, 78057, 166409)
replace nuclear = 2 if inlist(pid, 15019, 15020, 75655, 75656, 101203,		///
	101204, 151716, 151717, 151718)
replace nuclear = 3 if inlist(pid,  76614, 94007, 130731, 131305, 140402)

replace shared  = 1 if inlist(pid, 40372, 46951, 46952, 46953, 46954,		///
	49954, 50671, 58998, 81744, 97631, 102369, 115182, 115740, 116442, 		///
	117192, 117576, 122612, 129905, 136212, 171615, 180496)

replace extended  = 1 if inlist(pid, 40372, 46951, 46952, 46953, 46954,		///
	49954, 50671, 58998, 81744, 97631, 115182, 115740, 116442, 117192, 		///
	117576, 122612, 129905, 136212, 171615, 180496)

drop if inlist(pid, 44679)


* Nuclear Indicator variable with collapsed categories
recode nuclear (1/3 = 1), gen(nuclearind)
label var nuclearind "Nuclear Indicator"
label def nuclearind				///
	1 "Nuclear"
label val nuclearind nuclearind

* Living arrangement variable (nuclear and shared in one variable)
cap drop larrange
gen larrange = .
replace larrange = 1 if nuclearind == 1
replace larrange = 2 if shared == 1
label var larrange "Household Constellation"
label def larrange 				///
	1 "Nuclear"					///
	2 "Shared"
label val larrange larrange


/*------------------------------------------------------------------------------
Covariate Variable Preparation
------------------------------------------------------------------------------*/

**#

/*------------------------------------------------------------------------------
 Parental Variables
------------------------------------------------------------------------------*/

* Identifier variable for children
egen pick_c = tag(hhid hhmid) if age < 15
bysort hhid: gen c_id = sum(pick_c)
replace c_id = 0 if pick_c == 0

* (1) Education

* Maternal education level
cap drop aux 
gen mother_educat = .
sum c_id
quietly forvalues i = 1/`r(max)' {
        generate aux = 1 if age < 15 & motherhhmid != hhmid & motherhhmid < . & c_id == `i'
		egen aux1 = max(motherhhmid * aux), by(hhid) 
		by hhid: egen equal = max(educat) if hhmid == aux1
		by hhid: egen educat_mom = max(equal)
		replace mother_educat = educat_mom if c_id == `i' & aux == 1
		drop aux aux1 equal educat_mom
}
label var mother_educat "Maternal educational level"

* Paternal education level
cap drop aux 
gen father_educat = .
sum c_id
quietly forvalues i = 1/`r(max)' {
        generate aux = 1 if age < 15 & fatherhhmid != hhmid & fatherhhmid < . & c_id == `i'
		egen aux1 = max(fatherhhmid * aux), by(hhid) 
		by hhid: egen equal = max(educat) if hhmid == aux1
		by hhid: egen educat_dad = max(equal)
		replace father_educat = educat_dad if c_id == `i' & aux == 1
		drop aux aux1 equal educat_dad
}
label var father_educat "Paternal educational level"

* Parental education level (mother's; father's if mother's information unavaiable)
generate parent_educat = .
replace parent_educat = mother_educat if mother_educat < .
replace parent_educat = father_educat if mother_educat == . & father_educat < . 
label var parent_educat "Parental educational level"
label drop educat
label def educat 				///
	0 "No education"			///
	1 "Primary"					///
	2 "Secondary"				///
	3 "Higher"
label val educat mother_educat father_educat parent_educat educat	

* (2) Ethnicity

* Maternal ethnicity
cap drop aux 
gen mother_ethni = .
sum c_id
quietly forvalues i = 1/`r(max)' {
        generate aux = 1 if age < 15 & motherhhmid != hhmid & motherhhmid < . & c_id == `i'
		egen aux1 = max(motherhhmid * aux), by(hhid) 
		by hhid: egen equal = max(ethnicity) if hhmid == aux1
		by hhid: egen ethni_mom = max(equal)
		replace mother_ethni = ethni_mom if c_id == `i' & aux == 1
		drop aux aux1 equal ethni_mom
}
label var mother_ethni "Maternal ethnicity"

* Paternal ethnicity
cap drop aux 
gen father_ethni = .
sum c_id
quietly forvalues i = 1/`r(max)' {
        generate aux = 1 if age < 15 & fatherhhmid != hhmid & fatherhhmid < . & c_id == `i'
		egen aux1 = max(fatherhhmid * aux), by(hhid) 
		by hhid: egen equal = max(ethnicity) if hhmid == aux1
		by hhid: egen ethni_dad = max(equal)
		replace father_ethni = ethni_dad if c_id == `i' & aux == 1
		drop aux aux1 equal ethni_dad
}
label var father_ethni "Paternal ethnicity"

* Parental ethnicity (mother's; father's if mother's information unavaiable)
generate parent_ethni = .
replace parent_ethni = mother_ethni if mother_ethni < .
replace parent_ethni = father_ethni if mother_ethni == . & father_ethni < .
label var parent_ethni "Parental ethnicity"

label val mother_ethni father_ethni parent_ethni ethnicity

* (3) Denomination

* Maternal religion
cap drop aux 
gen mother_religion = .
sum c_id
quietly forvalues i = 1/`r(max)' {
        generate aux = 1 if age < 15 & motherhhmid != hhmid & motherhhmid < . & c_id == `i'
		egen aux1 = max(motherhhmid * aux), by(hhid) 
		by hhid: egen equal = max(religion) if hhmid == aux1
		by hhid: egen reli_mom = max(equal)
		replace mother_religion = reli_mom if c_id == `i' & aux == 1
		drop aux aux1 equal reli_mom
}
label var mother_religion "Maternal religion"

* Paternal religion
cap drop aux 
gen father_religion = .
sum c_id
quietly forvalues i = 1/`r(max)' {
        generate aux = 1 if age < 15 & fatherhhmid != hhmid & fatherhhmid < . & c_id == `i'
		egen aux1 = max(fatherhhmid * aux), by(hhid) 
		by hhid: egen equal = max(religion) if hhmid == aux1
		by hhid: egen reli_dad = max(equal)
		replace father_religion = reli_dad if c_id == `i' & aux == 1
		drop aux aux1 equal reli_dad
}
label var father_religion "Paternal religion"


* Parental religion (mother's; father's if mother's information unavaiable)
generate parent_religion = .
replace parent_religion = mother_religion if mother_religion < .
replace parent_religion = father_religion if mother_religion == . & father_religion < .
label var parent_religion "Parental religion"
						
label val mother_religion father_religion parent_religion religion


/*------------------------------------------------------------------------------
 Child Variables
------------------------------------------------------------------------------*/

**#

* (1) Age (Categories)
recode age 													///
	(0  / 4  = 1 "< 5")										///
	(5 /  9  = 2 "5-9")										///
	(10 / 14 = 3 "10-14")									///
	, gen(agecat2)
label variable agecat2 "Age Groups (Children)"


/*------------------------------------------------------------------------------
 Final Sample restrictions
------------------------------------------------------------------------------*/

**#

* Indicator for children living with at least one parent
gen withparent = .
replace withparent = 1 if 													///
	(fatherhhmid < . & fatherhhmid > 0) | 									///
	(motherhhmid < . & motherhhmid > 0)


* Keep only relevant variables
keep																		///
	wave hhid hhmid pid v001 v002 wgt stratum_ID clstr_ID					///
	hhpickone hh_size hh_nr_children										///
	age agecat agecat2 childind childind2 withparent						///
	nuclear nuclearind shared larrange extended grandparent					///
	region urbanrural 														///
	sex educat motheralive fatheralive										///
	wealthindexquint 														///
	parent_educat parent_ethni parent_religion
	

* Sample selection:
keep if childind2 == 1 	// only children below 15
bysort wave: distinct hhid
* Between 6677 and 18896 children in 3027 to 7496 households
distinct hhid 			// 75515 children in 29857 households

* Save below 15 sample
save "$posted/GDHS_below15", replace


/*-----------------------------------------------------------------------------
PART 2: ANALYSIS
------------------------------------------------------------------------------*/

**#

/*-----------------------------------------------------------------------------
II Sample Description and Missing Tables
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
IIa Sample Description (Table A2)
------------------------------------------------------------------------------*/

* Load analytic sample data	(below 15)
clear all             			
	use "$posted/GDHS_below15", clear

* (i) Generate dummy variables for categorical variables
	
* Child Age
tab agecat2, gen(a)
label var a1 "\hspace{0.25cm} < 5"
label var a2 "\hspace{0.25cm} 5-9"
label var a3 "\hspace{0.25cm} 10-14"

* Child Gender
tab sex, gen(g)
label var g1 "\hspace{0.25cm} Male"
label var g2 "\hspace{0.25cm} Female"

* Region
tab region, gen(r)
label var r1 "\hspace{0.25cm} Western"
label var r2 "\hspace{0.25cm} Central"
label var r3 "\hspace{0.25cm} Greater Accra"
label var r4 "\hspace{0.25cm} Volta"
label var r5 "\hspace{0.25cm} Eastern"
label var r6 "\hspace{0.25cm} Ashanti"
label var r7 "\hspace{0.25cm} Brong-Ahafo"
label var r8 "\hspace{0.25cm} Northern"
label var r9 "\hspace{0.25cm} Upper West"
label var r10 "\hspace{0.25cm} Upper East"

* Urban/rural
tab urbanrural, gen(u)
label var u1 "\hspace{0.25cm} Urban"
label var u2 "\hspace{0.25cm} Rural"

* Household Wealth Index Quintiles
tab wealthindexquint, gen(w)
label var w1 "\hspace{0.25cm} Lowest"
label var w2 "\hspace{0.25cm} Second"
label var w3 "\hspace{0.25cm} Middle"
label var w4 "\hspace{0.25cm} Fourth"
label var w5 "\hspace{0.25cm} Highest"

* Parental Ethnicity
tab parent_ethni, gen(pet)
label var pet1 "\hspace{0.25cm} Akan"
label var pet2 "\hspace{0.25cm} Ga/Adangbe"
label var pet3 "\hspace{0.25cm} Ewe"
label var pet4 "\hspace{0.25cm} Guan"
label var pet5 "\hspace{0.25cm} Mole-Dagbani"
label var pet6 "\hspace{0.25cm} Grussi"
label var pet7 "\hspace{0.25cm} Gruma"
label var pet8 "\hspace{0.25cm} Other"

* Parental Religion
tab parent_religion, gen(rel)
label var rel1 "\hspace{0.25cm} Christian"
label var rel2 "\hspace{0.25cm} Muslim"
label var rel3 "\hspace{0.25cm} Traditional"
label var rel4 "\hspace{0.25cm} None/other"

* Parental Education
tab parent_educat, gen(pe)
label var pe1 "\hspace{0.25cm} None"
label var pe2 "\hspace{0.25cm} Primary"
label var pe3 "\hspace{0.25cm} Secondary"
label var pe4 "\hspace{0.25cm} Higher"

* Number of observations (by wave)
bysort wave: gen N_wave = _N
label var N_wave "Individuals"

* (ii) Produce and export table 
eststo tableA2: estpost tabstat 												///
	a1 a2 a3 g1 g2 r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 u1 u2							///
	w1 w2 w3 w4 w5 pet1 pet2 pet3 pet4 pet5 									///
	pet6 pet7 pet8 rel1 rel2 rel3 rel4 pe1 pe2 pe3 pe4 					 		///
	N_wave																		///
	,by(wave)																	///
	statistics(mean sd count) nototal columns(statistics)
	
esttab tableA2 using "$table/tableA2.tex", compress label replace				///
	refcat(	a1 "\emph{Age}"														///
			g1 "\emph{Gender}"													///
			r1 "\emph{Region}"													///
			u1 "\emph{Type of place}"											///
			w1 "\emph{Household wealth}"										///
			pet1 "\emph{Parental ethnicity}"									///
			rel1 "\emph{Parental denomination}"									///
			pe1 "\emph{Parental education}"										///
			, nolabel) 															///	
	cells("Mean(fmt(2))") 														///
	collabels("M")																///
	wide not nostar unstack nonumber noobs nodepvars 							///
	title("")																	///
	note(Source: Ghana Demographic and Health Survey, 1993-2014; 				///
		Ghana Socioeconomic Panel Survey, 2018.)

/*-----------------------------------------------------------------------------
IIb Missing table (Table A1)
------------------------------------------------------------------------------*/	

* Wave variable
recode wave																		///
	(1993 = 1 "1993") (1998 = 2 "1998") (2003 = 3 "2003") 						///
	(2008 = 4 "2008") (2014 = 5 "2014") (2018 = 6 "2018")						///
	, gen(wavenum)		

* Loops to generate table entries	
foreach x of varlist agecat2 sex region urbanrural wealthindexquint 			///
		parent_ethni parent_religion parent_educat								///
	{
	gen `x'_miss_rf = 0
	replace `x'_miss_rf = 100 if missing(`x')
	label var `x'_miss_rf "\hspace{0.25cm} \% Missing"
	gen `x'_nomiss = .
	label var `x'_nomiss "\hspace{0.25cm} Nonmissing"
	gen `x'_miss = .
	label var `x'_miss "\hspace{0.25cm} Missing"
	}

foreach x of varlist agecat2 sex region urbanrural wealthindexquint 			///
		parent_ethni parent_religion parent_educat								///
	{
		forvalues i = 1/6 {
		replace `x'_miss_rf = 100 if missing(`x')
		egen `x'_nomiss_w`i' = count(`x'_miss_rf) if `x'_miss_rf == 0 			///
			& wavenum == `i'
		replace `x'_nomiss = `x'_nomiss_w`i' if wavenum == `i'
		egen `x'_miss_w`i' = count(`x'_miss_rf) if `x'_miss_rf == 100 			///
			& wavenum == `i'
		replace `x'_miss = `x'_miss_w`i' if wavenum == `i'
		}
	}

* Percentages for parental variables for children living with parent(s)	
foreach x of varlist parent_ethni parent_religion parent_educat 				///
	{
	gen `x'_miss_wp_rf = 0
	replace `x'_miss_wp_rf = 100 if missing(`x') & withparent == 1
	label var `x'_miss_wp_rf "\hspace{0.25cm} \% Missing"
	gen `x'_nomiss_wp = .
	label var `x'_nomiss_wp "\hspace{0.25cm} Nonmissing"
	gen `x'_miss_wp = .
	label var `x'_miss_wp "\hspace{0.25cm} Missing"
	}

foreach x of varlist parent_ethni parent_religion parent_educat 				///
	{
		forvalues i = 1/6 {
		replace `x'_miss_wp_rf = 100 if missing(`x') & withparent == 1
		egen `x'_nomiss_wp_w`i' = count(`x'_miss_wp_rf) if `x'_miss_wp_rf == 0 	///
			& wavenum == `i' & withparent == 1
		replace `x'_nomiss_wp = `x'_nomiss_wp_w`i' if wavenum == `i'
		egen `x'_miss_wp_w`i' = count(`x'_miss_wp_rf) if `x'_miss_wp_rf == 100 	///
			& wavenum == `i' & withparent == 1
		replace `x'_miss_wp = `x'_miss_wp_w`i' if wavenum == `i'
		}
	}	

* Correct % Missing for with parent parts
foreach x of varlist parent_ethni parent_religion parent_educat 				///
	{
		forvalues i = 1/6 {
		egen `x'_aux1_`i' = max(`x'_nomiss_wp) if wavenum == `i'
		egen `x'_aux2_`i' = max(`x'_miss_wp) if wavenum == `i'
		replace `x'_miss_wp_rf = 												///
		(`x'_aux2_`i' / (`x'_aux2_`i' + `x'_aux1_`i'))*100 if wavenum == `i'
		}	
	}	


* Produce and export table			
forvalues i = 1/6 {
	eststo miss_w`i': estpost tabstat 											///
	agecat2_nomiss agecat2_miss agecat2_miss_rf									///
	sex_nomiss sex_miss sex_miss_rf												///
	region_nomiss region_miss region_miss_rf									///
	urbanrural_nomiss urbanrural_miss urbanrural_miss_rf						///
	wealthindexquint_nomiss wealthindexquint_miss wealthindexquint_miss_rf		///
	parent_ethni_nomiss parent_ethni_miss parent_ethni_miss_rf					///
	parent_ethni_nomiss_wp parent_ethni_miss_wp parent_ethni_miss_wp_rf			///
	parent_religion_nomiss parent_religion_miss parent_religion_miss_rf			///
	parent_religion_nomiss_wp parent_religion_miss_wp 							///
	parent_religion_miss_wp_rf													///
	parent_educat_nomiss parent_educat_miss parent_educat_miss_rf				///
	parent_educat_nomiss_wp parent_educat_miss_wp parent_educat_miss_wp_rf		///
	N_wave																		///
		if wavenum == `i', by(wavenum)											///
	statistics(mean sd count) nototal columns(statistics)	
}	
	
	
esttab miss_w1 miss_w2 miss_w3 miss_w4 miss_w5 miss_w6 							///
	using "$table/misstable.tex", compress label replace						///	
	refcat(	agecat2_nomiss "\emph{Age}"											///
			sex_nomiss "\emph{Gender}"											///
			region_nomiss "\emph{Region}"										///
			urbanrural_nomiss "\emph{Type of place}"							///
			wealthindexquint_nomiss "\emph{Household wealth}"					///
			parent_ethni_nomiss	"\emph{Parental ethnicity}"						///
			parent_ethni_nomiss_wp	"\hspace{0.1cm} \emph{With parent(s)}"		///
			parent_religion_nomiss	"\emph{Parental religion}"					///
			parent_religion_nomiss_wp	"\hspace{0.1cm} \emph{With parent(s)}"	///
			parent_educat_nomiss "\emph{Parental education}"					///
			parent_educat_nomiss_wp "\hspace{0.1cm} \emph{With parent(s)}"		///
			, nolabel) 															///
	cells("Mean(fmt(2))") 														///
	wide not nostar unstack nonumber noobs nodepvars 							///
	title("")																	///
	note(Source: Ghana Demographic and Health Survey, 1993-2014; 				///
		Ghana Socioeconomic Panel Survey, 2018.)	

/*-----------------------------------------------------------------------------
II Trends in living arrangements
------------------------------------------------------------------------------*/

* Load analytic sample data	(below 15)
clear all             			
	use "$posted/GDHS_below15", clear

* Weight settings:	
svyset clstr_ID [pw = wgt], strata(stratum_ID) singleunit(centered)

* Wave variable
recode wave																		///
	(1993 = 1 "1993") (1998 = 2 "1998") (2003 = 3 "2003") 						///
	(2008 = 4 "2008") (2014 = 5 "2014") (2018 = 6 "2018")						///
	, gen(wavenum)	

* Set 1 to 100 and missing to 0 so that percentages are displayed on yaxis
recode nuclearind shared extended grandparent(. = 0) (1=100)

* Font settings	
graph set
graph set window fontface "Times New Roman"

* (i) Overall trends (Figure 1)

* Regression models to produce confidence intervals:
reg nuclearind i.wavenum [pw=wgt]
est store g_nuclear
margins wavenum, saving(mnuclear, replace)

reg shared i.wavenum [pw=wgt]
est store g_shared
margins wavenum, saving(mshared, replace)

reg extended i.wavenum [pw=wgt]
est store g_extended
margins wavenum, saving(mextended, replace)

reg grandparent i.wavenum [pw=wgt]
est store g_grandparent
margins wavenum, saving(mgrandparent, replace)

* Figure 1: Overall Trends
combomarginsplot mnuclear mshared mextended mgrandparent , 						///
 label("Nuclear" "Shared" "Extended" "Grandparents" )  							///
file1opts(lcolor(dknavy) msize() msymbol(s) mcolor(dknavy) 						///
	lpattern(solid) lwidth(medthick)) 											///
	fileci1opts(recast(rarea) color(dknavy%20) lwidth(thin)) 					///
file2opts(lcolor(teal) msize() msymbol(t) mcolor(teal) 							///
	lpattern(solid) lwidth(medthick)) 											///
	fileci2opts(recast(rarea) color(teal%20) lwidth(thin)) 						///
file3opts(lcolor(dkorange) msize() msymbol(o) mcolor(dkorange) 					///
	lpattern(solid) lwidth(medthick)) 											///
	fileci3opts(recast(rarea) color(dkorange%20) lwidth(thin)) 					///
file4opts(lcolor(maroon) msize() msymbol(d) mcolor(maroon) 						///
	lpattern(solid) lwidth(medthick)) 											///
	fileci4opts(recast(rarea) color(maroon%20) lcolor(gs3) lwidth(thin)) 		///
xtitle("")																		///
ytitle("Children < 15 (%)", size(small) color(gs3) margin(r+2))					///	
xlabel(, valuelabel labsize(small) labcolor(gs3))								///
ylabel(0(10)70, labsize(small) labcolor(gs3) angle(0) 							///
	glcolor(gs15) glwidth(thin)) 												///
plotregion(color(white)) 														///
graphregion(color(white) margin(vsmall) fcolor(white) ifcolor(white)) 			///
legend(size(medsmall) rows(1) region(color(white)) pos(6)) 						///
title("")
graph export "$graph/fig1.png", replace


* Table A3: Regression models (Overall)
esttab g_nuclear g_shared g_extended g_grandparent using "$table/table_A2.tex", ///
	replace label wide															///
	b(2) se(2) noomitted 														///
	refcat(	2.wavenum  "\emph{Year (ref: 1993)}", nolabel) 						///
	star(+ 0.10 * 0.05 ** 0.01) nobaselevels									///
	stats(r2 N, fmt(2 0) labels(R-squared "Individuals")) 						///
	mtitles("Nuclear" "Shared" "Extended" "Grandparent") nonumbers

* (ii) Trends by deographic subgroups	

* Match value label names with variable name for loop below to function	
label copy ethnicity parent_ethni, replace
label val parent_ethni parent_ethni

label copy religion parent_religion, replace
label val parent_religion parent_religion

label copy educat parent_educat, replace
label val parent_educat parent_educat

* Subgraphs for subgroups

* variables with 60 % y axis limit
quietly foreach x of varlist 													///
	agecat2 sex region urbanrural wealthindexquint 								///
	{
	sum `x'
		forvalues i = `r(min)'/`r(max)' {
		
		reg shared i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(shared_`x'_`i', replace)

		reg extended i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(extended_`x'_`i', replace)

		reg grandparent i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(grandparent_`x'_`i', replace) 
	
		local `x'_`i': label `x' `i'
	
	combomarginsplot shared_`x'_`i' extended_`x'_`i' grandparent_`x'_`i' , 		///
	label("Shared" "Extended" "Grandparents")  									///
	file1opts(lcolor(teal) msize() msymbol(t) mcolor(teal) 						///
		lpattern(solid) lwidth(medium)) 										///
		fileci1opts(recast(rarea) color(teal%20) lwidth(vvvthin)) 				///
	file2opts(lcolor(dkorange) msize() msymbol(o) mcolor(dkorange) 				///
		lpattern(solid) lwidth(medium)) 										///
		fileci2opts(recast(rarea) color(dkorange%20) lwidth(vvvthin)) 			///
	file3opts(lcolor(maroon) msize() msymbol(d) mcolor(maroon) 					///
		lpattern(solid) lwidth(medium)) 										///
		fileci3opts(recast(rarea) color(maroon%20)  lwidth(vvvthin)) 			///
	xtitle("")																	///
	ytitle("")																	///	
	xlabel(, valuelabel labsize(medsmall) labcolor(gs3))						///
	ylabel(0(10)60, labsize(medsmall) labcolor(gs3) angle(0) 					///
		glcolor(gs15) glwidth(thin) nogmin nogmax) 								///
	plotregion(color(white)) 													///
	graphregion(color(white) fcolor(white) ifcolor(white)) 						///
	legend(size(huge) rows(1) region(color(white)) pos(6)) 						///
	title("{bf:``x'_`i''}", size(large) color(gs3)) 							///
	saving(marginsplot3_`x'_`i', replace)
		
	}
}  

* variables with 50 % y axis limit
quietly foreach x of varlist 													///
	parent_ethni parent_religion 												///
	{
	sum `x'
		forvalues i = `r(min)'/`r(max)' {
		
		reg shared i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(shared_`x'_`i', replace)

		reg extended i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(extended_`x'_`i', replace)

		reg grandparent i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(grandparent_`x'_`i', replace) 
	
		local `x'_`i': label `x' `i'
	
	combomarginsplot shared_`x'_`i' extended_`x'_`i' grandparent_`x'_`i' , 		///
	label("Shared" "Extended" "Grandparents")  									///
	file1opts(lcolor(teal) msize() msymbol(t) mcolor(teal) 						///
		lpattern(solid) lwidth(medium)) 										///
		fileci1opts(recast(rarea) color(teal%20) lwidth(vvvthin)) 				///
	file2opts(lcolor(dkorange) msize() msymbol(o) mcolor(dkorange) 				///
		lpattern(solid) lwidth(medium)) 										///
		fileci2opts(recast(rarea) color(dkorange%20) lwidth(vvvthin)) 			///
	file3opts(lcolor(maroon) msize() msymbol(d) mcolor(maroon) 					///
		lpattern(solid) lwidth(medium)) 										///
		fileci3opts(recast(rarea) color(maroon%20)  lwidth(vvvthin)) 			///
	xtitle("")																	///
	ytitle("")																	///	
	xlabel(, valuelabel labsize(medsmall) labcolor(gs3))						///
	ylabel(0(10)50, labsize(medsmall) labcolor(gs3) angle(0) 					///
		glcolor(gs15) glwidth(thin) nogmin nogmax) 								///
	plotregion(color(white)) 													///
	graphregion(color(white) fcolor(white) ifcolor(white)) 						///
	legend(size(huge) rows(1) region(color(white)) pos(6)) 					///
	title("{bf:``x'_`i''}", size(large) color(gs3)) 							///
	saving(marginsplot3_`x'_`i', replace)
		
	}
}  

* Panels:

* Figure 2: Trends by child age
grc1leg2 marginsplot3_agecat2_1.gph marginsplot3_agecat2_2.gph 					///
	marginsplot3_agecat2_3.gph, 												///
		legendfrom(marginsplot3_agecat2_1.gph) 									///
		iscale(1) lmsize(large)													///
		col(3) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) ysize (1.5) 
graph export "$graph/fig2.png", replace	

* Figure 2: Trends by type of place
grc1leg2 marginsplot3_urbanrural_1.gph marginsplot3_urbanrural_2.gph, 			///
		legendfrom(marginsplot3_urbanrural_1.gph) 								///
		iscale(1) lmsize(large)													///
		col(3) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) ysize (1.5)
graph export "$graph/fig3.png", replace			
		
* Figure 3: Trends by parental education	
	* Set y-axis limit to 70
quietly foreach x of varlist 													///
	parent_educat 																///
	{
	sum `x'
		forvalues i = `r(min)'/`r(max)' {
		
		reg shared i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(shared_`x'_`i', replace)

		reg extended i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(extended_`x'_`i', replace)

		reg grandparent i.wavenum [pw=wgt] if `x' == `i'
		margins wavenum, saving(grandparent_`x'_`i', replace) 
	
		local `x'_`i': label `x' `i'
	
	combomarginsplot shared_`x'_`i' extended_`x'_`i' grandparent_`x'_`i' , 		///
	label("Shared" "Extended" "Grandparents")  									///
	file1opts(lcolor(teal) msize() msymbol(t) mcolor(teal) 						///
		lpattern(solid) lwidth(medium)) 										///
		fileci1opts(recast(rarea) color(teal%20) lwidth(vvvthin)) 				///
	file2opts(lcolor(dkorange) msize() msymbol(o) mcolor(dkorange) 				///
		lpattern(solid) lwidth(medium)) 										///
		fileci2opts(recast(rarea) color(dkorange%20) lwidth(vvvthin)) 			///
	file3opts(lcolor(maroon) msize() msymbol(d) mcolor(maroon) 					///
		lpattern(solid) lwidth(medium)) 										///
		fileci3opts(recast(rarea) color(maroon%20)  lwidth(vvvthin)) 			///
	xtitle("")																	///
	ytitle("")																	///	
	xlabel(, valuelabel labsize(medsmall) labcolor(gs3))						///
	ylabel(0(10)70, labsize(medsmall) labcolor(gs3) angle(0) 					///
		glcolor(gs15) glwidth(thin) nogmin nogmax) 								///
	plotregion(color(white)) 													///
	graphregion(color(white) fcolor(white) ifcolor(white)) 						///
	legend(size(medsmall) rows(1) region(color(white)) pos(6)) 					///
	title("{bf:``x'_`i''}", size(large) color(gs3)) 							///
	saving(marginsplot3_`x'_`i', replace)
		
	}
}  	

grc1leg2 marginsplot3_parent_educat_0.gph marginsplot3_parent_educat_1.gph  	///
		marginsplot3_parent_educat_2.gph marginsplot3_parent_educat_3.gph, 		///
		legendfrom(marginsplot3_parent_educat_0.gph) 							///
		iscale(1) lmsize(large)	labsize(vlarge)									///
		col(2) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) ysize (2.5)  scale(0.8) 
graph export "$graph/fig4.png", replace
		
* Figure A1: Trends by sex
grc1leg2 marginsplot3_sex_1.gph marginsplot3_sex_2.gph, 						///
		legendfrom(marginsplot3_sex_1.gph) 										///
		iscale(1) lmsize(large)													///
		col(3) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) ysize (1.5)
graph export "$graph/figA1.png", replace			
			
* Figure A2: Trends by region		
grc1leg2 marginsplot3_region_1.gph marginsplot3_region_2.gph					///
		marginsplot3_region_3.gph  marginsplot3_region_4.gph  					///
		marginsplot3_region_5.gph marginsplot3_region_6.gph 					///
		marginsplot3_region_7.gph marginsplot3_region_8.gph 					///
		marginsplot3_region_9.gph marginsplot3_region_10.gph, 					///
		legendfrom(marginsplot3_region_1.gph) 									///
		iscale(1) lmsize(large)	labsize(vlarge)									///
		col(3) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		scale(0.5) ysize(6)   
graph export "$graph/figA2.png", replace				
	
* Figure A3: Trends by parental ethnicity
grc1leg2 marginsplot3_parent_ethni_1.gph marginsplot3_parent_ethni_2.gph  		///
		marginsplot3_parent_ethni_3.gph  marginsplot3_parent_ethni_4.gph  		///
		marginsplot3_parent_ethni_5.gph marginsplot3_parent_ethni_6.gph 		///
		marginsplot3_parent_ethni_7.gph marginsplot3_parent_ethni_8.gph,		///
		legendfrom(marginsplot3_parent_ethni_1.gph) 							///
		iscale(1) lmsize(large)	labsize(vlarge)									///
		col(3) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) scale(0.6) ysize(2.5)   
graph export "$graph/figA3.png", replace

* Figure A4: Trends by household wealth
grc1leg2 marginsplot3_wealthindexquint_1.gph 									///
		marginsplot3_wealthindexquint_2.gph 									///
		marginsplot3_wealthindexquint_3.gph 									///
		marginsplot3_wealthindexquint_4.gph 									///
		marginsplot3_wealthindexquint_5.gph, 									///
		legendfrom(marginsplot3_wealthindexquint_1.gph) 						///
		iscale(1) lmsize(large)													///
		col(3) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) ysize (2.0) scale(0.8)
graph export "$graph/figA4.png", replace	

* Figure A5: Trends by parental religion
grc1leg2 marginsplot3_parent_religion_1.gph marginsplot3_parent_religion_2.gph  ///
		marginsplot3_parent_religion_3.gph marginsplot3_parent_religion_4.gph, 	///
		legendfrom(marginsplot3_parent_religion_1.gph) 							///
		iscale(1) lmsize(large)	labsize(vlarge)									///
		col(2) l1("Children < 15 (%)", size(medlarge)  margin(r-3)) 			///
		graphregion(color(white) margin(0) fcolor(white) ifcolor(white)) 		///
		xsize(3.5) ysize (2.5)  scale(0.8) 
graph export "$graph/figA5.png", replace


* Tables A3 - A11: Regression models (Interaction wave X covariates)
rename wealthindexquint wealthindex

quietly foreach x of varlist 													///
	agecat2 sex region urbanrural wealthindex									///
	 parent_ethni parent_religion parent_educat 								///
	{	
	    reg shared i.`x'##i.wavenum [pw=wgt]
		est store `x'_shared
	    
		reg extended i.`x'##i.wavenum [pw=wgt]
		est store `x'_extended
		
		reg grandparent i.`x'##i.wavenum [pw=wgt]
		est store `x'_grandparent
		
		esttab `x'_shared `x'_extended `x'_grandparent 							///
			using "$table/table_A_`x'.tex", 									///
		replace label wide														///
		b(2) se(2) noomitted													///
		star(+ 0.10 * 0.05 ** 0.01) nobaselevels								///
		stats(r2 N, fmt(2 0) labels(R-squared "Individuals")) 					///
		mtitles("Shared (\%)" "Extended (\%)" "Grandparent (\%)") nonumbers
	}

*-------------------------------------------------------------------------------	
exit